#!/usr/local/bin/python3

import argparse
import glob
import os
import re
import sys

from common import authentication
from common import communication
from common import delegation
from common import internet
from common import files


parser = argparse.ArgumentParser(
    description=(
        "Using the results from response_sorter.py, checks the responses of "
        "the URLs again and compares them to the previous results. Reports on "
        "any differences it finds."
    )
)
parser.add_argument(
    "file_suffix",
    help=("the suffix each of response_sorter.py's results files end with, "
          "and the prefix of the redirect_map file, if it exists. Default is "
          "'response_codes.txt'."),
    default="response_codes.txt"
)
parser.add_argument(
    "--path",
    help="the directory that contains the old response_sorter files."
)
parser.add_argument(
    "--nofollow", action="store_false",
    help=("indicate the script should not follow redirects. By default, the "
          "script will follow any 30x redirects to their conclusion.")
)
delegation.add_defaults_to_parser(
    parser, ['u', 'p', 'basic', 'digest', 'login', 'threads', 'o', 'textme']
)


def sort_responses(args):
    """
    Uses response_sorter.py's output files to compare the response codes
    that were received during that script's run to how the response codes
    are now.

    :param args: the dictionary of parsed arguments from argparse.
    """
    auth = authentication.get_auth_dict(args)

    path = args.path or os.getcwd()

    file = args.output or "changed_response_codes.txt"
    suffix_no_ext = files.get_filename(args.file_suffix)

    map_file = suffix_no_ext + "_redirect_map.txt"

    changed_map_filename = (
        args.output and files.get_filename(args.output) or suffix_no_ext
    )

    changed_map_file = changed_map_filename + "_changed_redirects.txt"

    if args.login:
        header = authentication.wait_for_login_with_requests()
    else:
        header = []

    changed_codes = []
    results_files = glob.glob("{0}/*{1}.txt".format(path, suffix_no_ext))

    if not results_files:
        msg = (
            "Unable to find any results files with the suffix '{}'!\n"
            "Please double-check the filename."
        ).format(args.file_suffix)
        print(msg)
        sys.exit(1)

    map_file_exists = os.path.exists(map_file)

    for response_file in results_files:
        urls = files.get_url_list(response_file)

        code_pat = ".*?/([0-9]{3}|ERR)[\w _\-\.]*?" + re.escape(
            args.file_suffix
        )

        code_match = re.match(code_pat, response_file)

        if code_match:
            prev_code = code_match.group(1)
        else:
            # Must have grabbed a non response-code file.
            continue

        if prev_code != "ERR":
            prev_code = int(prev_code)

        responses = delegation.get_with_threaded_map(
            lambda url: internet.get_response_safely(url, auth, header),
            urls,
            num_threads=int(args.threads)
        )

        for origin, url, code in responses:
            if code != prev_code:
                changed_codes.append(
                    {
                        'url': url,
                        'code': code,
                        'prev_code': prev_code
                    }
                )

    changed_redirects = []

    if map_file_exists:
        with open(map_file, "r") as redirect_urls:
            for line in redirect_urls:
                line = line.strip()

                ori, old_dest = line.split(" > ")

                o, new_dest, r = internet.get_response_safely(
                    ori, auth, header
                )

                if old_dest != new_dest:
                    changed_redirects.append((ori, old_dest, new_dest))

        if len(changed_redirects) > 0:
            with open(changed_map_file, 'w+') as map_output:
                for origin, old_dest, new_dest in sorted(changed_redirects):
                    map_output.write(
                        "{0}   WAS: {1}   NOW: {2}\n".format(
                            origin,
                            old_dest,
                            new_dest
                        )
                    )

    num_changed_responses = len(changed_codes)
    num_changed_redirects = len(changed_redirects)

    msg = (
        "Response compare complete! {0} responses and {1} redirects had "
        "changed."
    )

    msg = msg.format(num_changed_responses, num_changed_redirects)

    if num_changed_responses > 0:
        changed_codes = sorted(changed_codes, key=lambda x: str(x['code']))

        with open(file, "w+") as output_file:
            for data in changed_codes:
                line = "{prev_code} -> {code}    {url}\n".format(**data)
                output_file.write(line)

        msg += " Results can be found in '{1}'"
        msg = msg.format(len(changed_codes), file)

        if num_changed_redirects:
            msg += " and"

    elif num_changed_redirects > 0:
        msg += " Results can be found"

    if num_changed_redirects > 0:
        msg += " in '{0}'.".format(changed_map_file)
    else:
        msg += "."

    if args.textme:
        communication.send_text(args.textme, msg)

    print(msg)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        args = parser.parse_args()
        sort_responses(args)
    else:
        parser.print_help()
else:
    print("This module is meant to be used as a standalone module.")
