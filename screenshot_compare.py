#!/usr/local/bin/python3


import argparse
import os
import re
import subprocess
import sys
from time import sleep

from common import authentication
from common import delegation
from common import files
from common import internet
from common import strings
from common import urls


parser = argparse.ArgumentParser(
    description=(
        "Using a file of URLs, the new_domain, and the old_domain, appends "
        "the path of the URL to new_domain and old_domain and compares "
        "screenshots from both of the resulting URLs. Creates images showing "
        "where the pixels were different. You can also specify a directory "
        "where old screenshots are kept instead of the old_domain."
    )
)
parser.add_argument(
    "url_file",
    help=("the file of URLs to check. The paths will be stripped from the "
          "URLs and appended to new_domain and old_domain")
)
parser.add_argument(
    "old_domain",
    help=("the first domain to insert before the URL paths, which will give "
          "us the first page of the two to compare.")
)
parser.add_argument(
    "new_domain",
    help=("the second domain to insert before the URL paths, which will give "
          "us the second page of the two to compare.")
)
parser.add_argument(
    "--add",
    help=("specifies a value to add to the end of each subdomain before they "
          "are crafted into new_domain's URL.")
)
parser.add_argument(
    "--remove",
    help=("specifies a value to be removed from each subdomain before they "
          "are crafted into new_domain's URL.")
)
parser.add_argument(
    "--replace", action='append',
    help=("specifies a value to replace with another value in each subdomain "
          "before they are crafted into new_domain's URL. These are entered "
          "as comma pairs (like 'this,that'), and many can be specified with "
          "multiple uses of this flag.")
)
parser.add_argument(
    "--verbatim", action='store_true',
    help=("indicates that the new_domain and old_domain should be kept as "
          "they were passed in entirely, and the script shouldn't split up "
          "the subdomain.")
)
parser.add_argument(
    "-f", "--files", action='store_true',
    help=("include this flag if old_domain is actually the path to a folder "
          "of screenshots to compare against new_domain. The screenshots must "
          "be of the pattern 'URLPATH_YYYY-MMM-DD_screengrab.png', as made "
          "by screenshot_grabber.py")
)
parser.add_argument(
    "-c", "--color", default="pink",
    help=("what color to use to highlight any differences in the screenshots. "
          "Default is Pink.")
)
delegation.add_defaults_to_parser(
    parser,
    ['u', 'p', 'basic', 'digest', 'login', 'wait',
     'height', 'width', 'o', 'textme']
)


def grab_screenshot(driver, url, name, wait=None):
    """
    A helper to go to a URL and grab a screenshot.

    :param driver: the driver to use to go to the page.
    :param url: the URL to go to.
    :param name: the name of the screenshot.
    """
    driver.get(url)

    if wait:
        sleep(wait)

    driver.get_screenshot_as_file(name)


def test_screenshots(args):
    """
    Compares screenshots using the provided information.

    :param args: the dictionary of parsed arguments from argparse.
    """
    url_list = files.get_url_list(args.url_file)

    auth = authentication.get_auth_dict(args)

    old_filename = "old_site.png"
    new_filename = "new_site.png"

    screengrab_pat = "_[0-9]{4}-[a-zA-Z]{3}-[0-9]{2}_screengrab.png"

    wait = float(args.wait) if args.wait else None

    output_path = args.output or ''

    # Templates formatted in for-loop below
    diff_template = output_path + "{url}_annotated_diff.png"
    url_template = "{protocol}://{subdomain}.{dom}{path}"
    verbatim_template = "{dom}{path}"

    if args.verbatim:
        new_domain = strings.add_terminating_slash(args.new_domain)
    else:
        new_domain = urls.get_base(args.new_domain)['domain']

    if args.files or args.verbatim:
        old_domain = strings.add_terminating_slash(args.old_domain)
    else:
        old_domain = urls.get_base(args.old_domain)['domain']

    print("Comparing", old_domain, "to", new_domain, "...")

    if args.files:
        file_list = os.listdir(old_domain)

        if not file_list:
            print("No files found in the specified domain!")
            sys.exit(1)

        try:
            file_list.remove(".DS_Store")  # can cause issues i guess?
        except ValueError:
            pass  # .DS_Store file wasn't there

    driver = internet.get_driver(args, args.new_domain)

    for url in url_list:
        url_info = urls.get_base(url)

        diff_filename = diff_template.format(
            url=files.get_filename_from_url(url))

        if args.files:
            url_filepath = files.get_filename_from_url(url)
            old_pat = re.escape(url_filepath) + screengrab_pat

            for filename in file_list:
                if re.match(old_pat, filename):
                    old_filename = old_domain + filename
                    break

            else:
                print("Screenshot not found: " + url_filepath)
                continue
        else:
            if args.verbatim:
                old_url = verbatim_template.format(dom=old_domain, **url_info)
            else:
                old_url = url_template.format(dom=old_domain, **url_info)

            if auth:
                old_url = urls.create_basic_auth_url(
                    old_url,
                    auth['username'],
                    auth['password']
                )

            grab_screenshot(driver, old_url, old_filename, wait=wait)

        if args.verbatim:
            new_url = verbatim_template.format(dom=new_domain, **url_info)

        else:
            if args.remove:
                subd = url_info['subdomain']
                url_info['subdomain'] = subd.replace(args.remove, '')

            if args.replace:
                for rep in args.replace:
                    before = rep.split(',')[0]
                    after = rep.split(',')[1]

                    subd = url_info['subdomain']

                    url_info['subdomain'] = subd.replace(before, after)

            if args.add:
                url_info['subdomain'] += args.add

            new_url = url_template.format(dom=new_domain, **url_info)

        if auth:
            new_url = urls.create_basic_auth_url(
                new_url,
                auth['username'],
                auth['password']
            )

        grab_screenshot(driver, new_url, new_filename, wait=wait)

        cmp_cmd = ["cmp", old_filename, new_filename]
        cmp_result = subprocess.call(cmp_cmd)

        if cmp_result == 1:
            gm_cmd = [
                "gm", "compare",
                "-highlight-style", "assign",
                "-highlight-color", args.color,
                "-file", diff_filename,
                old_filename, new_filename
            ]

            subprocess.call(gm_cmd)

        if not args.files:
            os.remove(old_filename)

        os.remove(new_filename)

    driver.quit()

    print('Done!')


if __name__ == "__main__":
    if len(sys.argv) > 3:
        args = parser.parse_args()
        test_screenshots(args)
    else:
        parser.print_help()
else:
    print("This module is meant to be used as a standalone script.")
