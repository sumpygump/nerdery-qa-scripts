xcode-select --install  # gets us `make` and other command-line tools

# xcode-select forks its install process, so the above command is non-blocking
echo "Press 'Enter' when the xcode command-line tools installation is finished."
read cont

# Check that Homebrew is installed, install it if it's not
hash brew 2>/dev/null || { /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"; }

# brew install graphicsmagick  # for screenshot_compare.py (currently deprecated)
brew install node            # for Accessibility aXe tests
brew install chromedriver    # for using Chrome webdriver
brew install geckodriver     # for using Firefox webdriver
brew install python3         # python3 is not installed by default
brew link python3 --force    # add python3 to /usr/local/bin

pip3 install -r requirements.txt --user
npm install axe-cli -g
