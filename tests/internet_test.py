import unittest
from unittest.mock import patch, MagicMock

from requests.exceptions import ConnectionError

from common import internet


class TestInternet(unittest.TestCase):
    """Internet"""

    @staticmethod
    def get_mocked_requests(url="", redirect_url="", source=""):
        """
        Sets up a mocked requests.

        :returns: a mocked requests object.
        """
        mocked_requests = MagicMock()

        if url == redirect_url == "":
            mocked_requests.get.side_effect = ConnectionError()

        else:
            mocked_get_response = MagicMock()
            mocked_head_response = MagicMock()

            mocked_get_response.status_code = 200
            mocked_get_response.url = redirect_url
            mocked_get_response.content = source

            mocked_head_response.status_code = 301
            mocked_head_response.url = url
            mocked_head_response.content = source

            mocked_requests.get.return_value = mocked_get_response
            mocked_requests.head.return_value = mocked_head_response

        return mocked_requests

    def test_source_is_html(self):
        """identifies HTML source string"""
        self.assertTrue(internet.source_is_html("<html></html>"))

    def test_source_is_not_html(self):
        """rejects non-HTML source string"""
        self.assertFalse(internet.source_is_html("blah"))

    def test_source_is_blank(self):
        """rejects blank source string"""
        self.assertFalse(internet.source_is_html(""))

    def test_source_is_not_string(self):
        """rejects non-string source"""
        self.assertFalse(internet.source_is_html(3))

    def test_get_response_safely(self):
        """gets response information"""
        url = "http://www.nintendo.com"
        redirect_url = "http://www.zelda.com"

        mocked_requests = TestInternet.get_mocked_requests(
            redirect_url=redirect_url
        )

        with patch("common.internet.requests", mocked_requests):
            self.assertEqual(
                internet.get_response_safely(url),
                [url, redirect_url, 200]
            )

    def test_get_response_safely_redirect(self):
        """gets redirect response information"""
        url = "http://www.nintendo.com"

        mocked_requests = TestInternet.get_mocked_requests(
            url=url
        )

        with patch("common.internet.requests", mocked_requests):
            self.assertEqual(
                internet.get_response_safely(url, follow=False),
                [url, url, 301]
            )

    def test_get_response_safely_exception(self):
        """handles exceptions when getting response"""
        url = "http://www.nintendo.com"

        mocked_requests = TestInternet.get_mocked_requests()

        with patch("common.internet.requests", mocked_requests):
            self.assertEqual(
                internet.get_response_safely(url),
                [url, url, "ERR"]
            )

    def test_fix_iframe_source(self):
        """fixes iframe source code"""
        self.assertEqual(
            internet.fix_iframe_source("<iframe>&lt;body/&gt;</iframe>"),
            "<iframe><body/></iframe>"
        )
