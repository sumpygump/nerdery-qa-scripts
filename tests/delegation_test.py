import unittest

from common import delegation


class TestDelegation(unittest.TestCase):
    """Delegation"""

    def test_get_with_threaded_map(self):
        """creates the correct collection with a threaded map"""
        self.assertEqual(
            delegation.get_with_threaded_map(
                lambda x: x + 1,
                range(10)
            ),
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        )
