import unittest
from unittest.mock import patch, MagicMock

from common import authentication


class TestAuthentication(unittest.TestCase):
    """Authentication"""

    def test_create_basic_auth_url(self):
        """creates basic auth URL"""
        self.assertEqual(
            authentication.create_basic_auth_url(
                "http://www.nintendo.com",
                "testuser",
                "12345"
            ),
            "http://testuser:12345@www.nintendo.com"
        )

    def test_create_basic_auth_url_with_path(self):
        """creates basic auth URL, remembering its path"""
        self.assertEqual(
            authentication.create_basic_auth_url(
                "http://www.nintendo.com/switch",
                "testuser",
                "12345"
            ),
            "http://testuser:12345@www.nintendo.com/switch"
        )

    @patch('common.authentication.get_user_password', return_value='12345')
    def test_get_auth_dict_none(self, input):
        """returns an auth dict of None"""
        mockArgs = MagicMock()

        setattr(mockArgs, 'username', '')
        setattr(mockArgs, 'password', False)

        self.assertIsNone(authentication.get_auth_dict(mockArgs))

    @patch('common.authentication.get_user_password', return_value='12345')
    def test_get_auth_dict_basic(self, input):
        """creates a BASIC auth dict."""
        mockArgs = MagicMock()

        setattr(mockArgs, 'username', 'testuser')
        setattr(mockArgs, 'password', True)
        setattr(mockArgs, 'digest', False)

        self.assertEqual(
            authentication.get_auth_dict(mockArgs),
            {
                'authtype': 'BASIC',
                'username': 'testuser',
                'password': '12345'
            }
        )

    @patch('common.authentication.get_user_password', return_value='12345')
    def test_get_auth_dict_digest(self, input):
        """creates a DIGEST auth dict."""
        mockArgs = MagicMock()

        setattr(mockArgs, 'username', 'testuser')
        setattr(mockArgs, 'password', True)
        setattr(mockArgs, 'digest', True)

        self.assertEqual(
            authentication.get_auth_dict(mockArgs),
            {
                'authtype': 'DIGEST',
                'username': 'testuser',
                'password': '12345'
            }
        )
