import unittest
from unittest.mock import patch, mock_open

from common import files
from common.exceptions import NoMatchFoundError


class TestFiles(unittest.TestCase):
    """Files"""

    @staticmethod
    def get_sample_url_file():
        """
        Creates a sample url file for testing functions that open files.

        :return: a MagicMock "file".
        """
        mock_file = MagicMock()
        mock_file.__iter__.return_value = [
            "http://www.nintendo.com/",
            "http://www.nintendo.com/switch",
            "http://www.nintendo.com/metroid"
        ]

        return mock_file

    def test_get_filename(self):
        """removes the filename's extension"""
        self.assertEqual(
            files.get_filename("metroid4.mov"),
            "metroid4"
        )

    def test_get_filename_no_extension(self):
        """does not modify a filename with no extension"""
        self.assertEqual(
            files.get_filename("metroid4"),
            "metroid4"
        )

    def test_get_url_list_url(self):
        """returns a single url as a list"""
        self.assertEqual(
            files.get_url_list("http://www.nintendo.com/"),
            ["http://www.nintendo.com/"]
        )

    def test_get_url_list_file(self):
        """returns a file of urls as a list"""
        test_data = """http://www.nintendo.com/
http://www.nintendo.com/switch
http://www.nintendo.com/metroid
"""
        mocked_open = mock_open(read_data=test_data)
        mocked_open.return_value.__iter__ = lambda self: self
        mocked_open.return_value.__next__ = lambda self: next(
            iter(self.readline, '')
        )

        with patch("common.files.open", mocked_open, create=True):
            self.assertEqual(
                files.get_url_list("testurls.txt"),
                [
                    "http://www.nintendo.com/",
                    "http://www.nintendo.com/switch",
                    "http://www.nintendo.com/metroid"
                ]
            )

    def test_get_filename_from_url(self):
        """creates a filename from a url"""
        self.assertEqual(
            files.get_filename_from_url(
                "http://www.nintendo.com/consoles/switch?games=metroid"
            ),
            "www_-consoles-switch?games=metroid"
        )

    def test_get_url_from_filename(self):
        """creates a url from a filename"""
        self.assertEqual(
            files.get_url_from_filename(
                "www_-consoles-switch?games=metroid_timestamp.png",
                "nintendo.com"
            ),
            "http://www.nintendo.com/consoles/switch?games=metroid"
        )

    def test_get_url_from_filename_exception(self):
        """raises a NoMatchFoundError when no url is found"""
        self.assertRaises(
            NoMatchFoundError,
            lambda: files.get_url_from_filename("foo", "bar")
        )
