import unittest

from common import strings


class TestStrings(unittest.TestCase):
    """Strings"""

    def test_add_terminating_slash(self):
        """adds a terminating slash"""
        self.assertEqual(
            strings.add_terminating_slash("blah-and-stuff"),
            "blah-and-stuff/"
        )

    def test_add_terminating_slash_with_extra_slash(self):
        """adds a terminating slash when another slash is in middle"""
        self.assertEqual(
            strings.add_terminating_slash("blah/and-stuff"),
            "blah/and-stuff/"
        )

    def test_do_not_add_extra_terminating_slash(self):
        """does not add an extra terminating slash"""
        self.assertEqual(
            strings.add_terminating_slash("blah-and-stuff/"),
            "blah-and-stuff/"
        )

    def test_remove_terminating_slash(self):
        """removes a terminating slash"""
        self.assertEqual(
            strings.remove_terminating_slash("blah-and-stuff/"),
            "blah-and-stuff"
        )

    def test_remove_terminating_slash_with_extra_slash(self):
        """removes a terminating slash when another slash is in middle"""
        self.assertEqual(
            strings.remove_terminating_slash("blah/and-stuff/"),
            "blah/and-stuff"
        )

    def test_do_not_remove_non_terminating_slash(self):
        """does not change string when there is no terminating slash"""
        self.assertEqual(
            strings.remove_terminating_slash("blah/and-stuff"),
            "blah/and-stuff"
        )

    def test_escape_braces(self):
        """escapes curly braces"""
        self.assertEqual(
            strings.escape_braces("this one {} and that one {}"),
            "this one {{}} and that one {{}}"
        )

    def test_escape_braces_without_braces(self):
        """does not change a string with no curly braces"""
        self.assertEqual(
            strings.escape_braces("a clean string!"),
            "a clean string!"
        )

    def test_create_tag_string(self):
        """creates a simple tag string"""
        self.assertEqual(
            strings.create_tag_string("div", []),
            "<div>"
        )

    def test_create_complicated_tag_string(self):
        """creates a simple tag string"""
        self.assertEqual(
            strings.create_tag_string(
                "div",
                [('class', 'disabled'), ('role', 'main')]
            ),
            '<div class="disabled" role="main">'
        )
