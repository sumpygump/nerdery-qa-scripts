import unittest

from common.parsers import AccessibilityParser
from common.parsers import LinkParser
from common.parsers import ObjectParser


class TestLinkParser(unittest.TestCase):
    """Link Parser"""

    def test_parser_finds_links(self):
        """can find a link"""
        test_html = """
        <html>
            <body>
                <a href="http://www.nintendo.com">
            </body>
        </html
        """
        parser = LinkParser("http://www.nerdery.com")
        parser.feed(test_html)

        self.assertEqual(parser.links, ["http://www.nintendo.com"])

    def test_parser_does_not_find_non_links(self):
        """ignores non-links"""
        test_html = """
        <html>
            <body>
                <img src="http://www.nintendo.com">
                <p>Stuff</p>
                <script src="http://www.nintendo.com">
            </body>
        </html
        """

        parser = LinkParser("http://www.nerdery.com")
        parser.feed(test_html)

        self.assertEqual(parser.links, [])

    def test_parser_quotes_urls(self):
        """quotes special characters in urls"""
        test_html = """
        <html>
            <body>
                <a href="/this url man?query=string">
            </body>
        </html
        """

        parser = LinkParser("http://www.nerdery.com")
        parser.feed(test_html)

        self.assertEqual(parser.links, ["/this%20url%20man?query=string"])


class TestObjectParser(unittest.TestCase):
    """Object Parser"""

    def test_parser_finds_with_tag(self):
        """can find an object with tag"""
        test_html = """
        <html>
            <body>
                <div class="test"></div>
            </body>
        </html
        """

        parser = ObjectParser("div", None, False, False, False)
        parser.feed(test_html)

        self.assertEqual(parser.bad_objects, ['class="test"'])

    def test_parser_finds_with_pairs(self):
        """can find an object with pairs"""
        test_html = """
        <html>
            <body>
                <div foo="bar"></div>
                <div foo="bar baz"></div>
            </body>
        </html
        """

        parser = ObjectParser("div", [['foo', 'bar']], False, False, False)
        parser.feed(test_html)

        self.assertEqual(parser.bad_objects, ['foo="bar"'])

    def test_parser_finds_with_pairs_and_partial(self):
        """can find an object with partial-matched pairs"""
        test_html = """
        <html>
            <body>
                <div foo="bar"></div>
                <div foo="bar baz"></div>
            </body>
        </html
        """

        parser = ObjectParser("div", [['foo', 'bar']], False, True, False)
        parser.feed(test_html)

        self.assertEqual(parser.bad_objects, ['foo="bar"', 'foo="bar baz"'])

    def test_parser_finds_with_inverted_pairs(self):
        """can find an object without pairs"""
        test_html = """
        <html>
            <body>
                <div foo="bar"></div>
                <div foo="bar baz"></div>
            </body>
        </html
        """

        parser = ObjectParser("div", [['foo', 'bar']], False, False, True)
        parser.feed(test_html)

        self.assertEqual(parser.bad_objects, ['foo="bar baz"'])

    def test_parser_finds_with_inverted_partial_pairs(self):
        """can find an object without partial-matched pairs"""
        test_html = """
        <html>
            <body>
                <div foo="bar"></div>
                <div foo="bar baz"></div>
                <div foo="blah"></div>
                <div class="test"></div>
            </body>
        </html
        """

        parser = ObjectParser("div", [['foo', 'bar']], False, True, True)
        parser.feed(test_html)

        self.assertEqual(parser.bad_objects, ['foo="blah"', 'class="test"'])

    def test_parser_finds_with_content(self):
        """can find an object with content"""
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div class="test">No stuff.</div>
            </body>
        </html
        """

        parser = ObjectParser("div", None, ["Stuff!"], False, False)
        parser.feed(test_html)

        self.assertEqual(parser.bad_objects, ['<div foo="bar">Stuff!</div>'])

    def test_parser_finds_with_inverted_content(self):
        """can find an object without content"""
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div class="test">No stuff.</div>
            </body>
        </html
        """

        parser = ObjectParser("div", None, ["Stuff!"], False, True)
        parser.feed(test_html)

        self.assertEqual(
            parser.bad_objects,
            ['<div class="test">No stuff.</div>']
        )

    def test_parser_finds_with_partial_content(self):
        """can find an object with partial-matched content"""
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div class="test">No stuff.</div>
            </body>
        </html
        """

        parser = ObjectParser("div", None, ["tuff"], True, False)
        parser.feed(test_html)

        self.assertEqual(
            parser.bad_objects,
            [
                '<div foo="bar">Stuff!</div>',
                '<div class="test">No stuff.</div>'
            ]
        )

    def test_parser_finds_with_inverted_partial_content(self):
        """can find an object with inverted partial-matched content"""
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div class="test">No stuff.</div>
                <div class="findme">Intentionally left blank!</div>
            </body>
        </html
        """

        parser = ObjectParser("div", None, ["tuff"], True, True)
        parser.feed(test_html)

        self.assertEqual(
            parser.bad_objects,
            [
                '<div class="findme">Intentionally left blank!</div>',
            ]
        )

    def test_parser_finds_with_pairs_and_content(self):
        """can find an object with content and pairs"""
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div class="test">No stuff.</div>
                <div class="findme">Intentionally left blank!</div>
            </body>
        </html
        """

        parser = ObjectParser(
            "div", [['foo', 'bar']], ["Stuff!"], False, False
        )
        parser.feed(test_html)

        self.assertEqual(parser.bad_objects, ['<div foo="bar">Stuff!</div>'])

    def test_parser_finds_with_partial_pairs_and_content(self):
        """can find an object with partial-matched content and pairs"""
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div class="test">No stuff.</div>
                <div class="findme">Intentionally left blank!</div>
            </body>
        </html
        """

        parser = ObjectParser(
            "div", [['class', 'find']], "Intentional", True, False
        )
        parser.feed(test_html)

        self.assertEqual(
            parser.bad_objects,
            [
                '<div class="findme">Intentionally left blank!</div>'
            ]
        )

    def test_parser_finds_with_inverted_pairs_and_content(self):
        """can find an object with inverted content and pairs"""
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div foo="bar">No stuff.</div>
                <div class="findme">Intentionally left blank!</div>
            </body>
        </html
        """

        parser = ObjectParser(
            "div", [['foo', 'bar']], ["Stuff!"], False, True
        )
        parser.feed(test_html)

        self.assertEqual(
            parser.bad_objects,
            [
                '<div class="findme">Intentionally left blank!</div>'
            ]
        )

    def test_parser_finds_with_partial_inverted_pairs_and_content(self):
        """
        can find an object with inverted partial-matched content and pairs
        """
        test_html = """
        <html>
            <body>
                <div foo="bar">Stuff!</div>
                <div foo="barra">Intentionally left blank!</div>
                <div class="findme">No stuff.</div>
                <div bar="baz">This one.</div>
            </body>
        </html
        """

        parser = ObjectParser(
            "div", [['foo', 'bar']], ["tuff"], True, True
        )
        parser.feed(test_html)

        self.assertEqual(
            parser.bad_objects,
            [
                '<div bar="baz">This one.</div>'
            ]
        )


class TestAccessibilityParser(unittest.TestCase):
    """Accessibility Parser"""

    def test_parser_finds_image(self):
        """detects an image"""
        img_src = (
            "http://www.zelda.com/"
            "breath-of-the-wild/assets/media/wallpapers/desktop-5.jpg"
        )

        test_html = """
        <html>
            <body>
                <img src="{0}">
            </body>
        </html>
        """.format(img_src)

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertEqual(len(parser.img_data), 1)
        self.assertEqual(parser.img_data[0]['attrs']['src'], img_src)

    def test_parser_ignores_hidden_image(self):
        """ignores hidden images"""
        img_src = (
            "http://www.zelda.com/"
            "breath-of-the-wild/assets/media/wallpapers/desktop-5.jpg"
        )

        test_html = """
        <html>
            <body>
                <img src="{0}" style="display: none">
                <img src="{0}" style="visibility: hidden">
                <img src="{0}" aria-hidden="true">
            </body>
        </html>
        """.format(img_src)

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertEqual(len(parser.img_data), 0)

    def test_parser_finds_image_in_iframe(self):
        """finds images in iframes"""
        img_src = (
            "http://www.zelda.com/"
            "breath-of-the-wild/assets/media/wallpapers/desktop-5.jpg"
        )

        test_html = """
        <html>
            <body>
                <iframe>
                    <html>
                        <body>
                            <img src="{0}">
                        </body>
                    </html>
                </iframe>
            </body>
        </html>
        """.format(img_src)

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertEqual(len(parser.img_data), 1)
        self.assertEqual(parser.img_data[0]['attrs']['src'], img_src)

    def test_parser_ignores_images_in_hidden_elements(self):
        """ignores images in hidden elements"""
        img_src = (
            "http://www.zelda.com/"
            "breath-of-the-wild/assets/media/wallpapers/desktop-5.jpg"
        )

        test_html = """
        <html>
            <body>
                <div style="display: none">
                    <img src="{0}">
                </div>
                <iframe style="visibility: hidden">
                    <html>
                        <body>
                            <img src="{0}">
                        </body>
                    </html>
                </iframe>
                <span aria-hidden="true">
                    <img src="{0}">
                </span>
            </body>
        </html>
        """.format(img_src)

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertEqual(len(parser.img_data), 0)

    def test_parser_identifies_pre_html5_aria_landmarks(self):
        """identifies pre-HTML5-style ARIA landmarks"""
        test_html = """
        <html>
            <body>
                <div role="main"></div>
                <div role="banner"></div>
                <div role="search"></div>
                <div role="navigation"></div>
                <div role="contentinfo"></div>
            </body>
        </html>
        """

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertTrue(parser.role_main)
        self.assertTrue(parser.role_banner)
        self.assertTrue(parser.role_search)
        self.assertTrue(parser.role_nav)
        self.assertTrue(parser.role_contentinfo)

    def test_parser_identifies_html5_aria_landmarks(self):
        """identifies HTML5 ARIA landmarks"""
        test_html = """
        <html>
            <body>
                <main role="main"></main>
                <header role="banner"></header>
                <form role="search"></form>
                <nav role="navigation"></nav>
                <footer role="contentinfo"></footer>
            </body>
        </html>
        """

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertTrue(parser.role_main)
        self.assertTrue(parser.role_banner)
        self.assertTrue(parser.role_search)
        self.assertTrue(parser.role_nav)
        self.assertTrue(parser.role_contentinfo)

    def test_parser_finds_elements_out_of_aria_landmarks(self):
        """finds elements that are outside of ARIA landmarks"""
        test_html = """
        <html>
            <body>
                <main role="main"></main>
                <header role="banner"></header>
                <form role="search"></form>
                <nav role="navigation"></nav>
                <footer role="contentinfo"></footer>
                <div class="test"></div>
            </body>
        </html>
        """

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertTrue(parser.role_main)
        self.assertTrue(parser.role_banner)
        self.assertTrue(parser.role_search)
        self.assertTrue(parser.role_nav)
        self.assertTrue(parser.role_contentinfo)
        self.assertEqual(parser.out_of_landmarks_data, ['<div class="test">'])

    def test_parser_ignores_blacklisted_aria_landmarks(self):
        """identifies HTML5 ARIA landmarks"""
        test_html = """
        <html>
            <body>
                <audio role="main"></audio>
                <address role="main"></address>
                <aside role="main"></aside>
                <section role="main"></section>
                <address role="banner"></address>
                <button role="banner"></button>
                <article role="banner"></article>
                <address role="search"></address>
                <article role="search"></article>
                <h1 role="search"></h1>
                <address role="navigation"></address>
                <object role="navigation"></object>
                <article role="navigation"></article>
                <aside role="navigation"></aside>
                <section role="navigation"></section>
                <article role="contentinfo"></article>
                <select role="contentinfo"></select>
                <aside role="contentinfo"></aside>
                <section role="contentinfo"></section>
            </body>
        </html>
        """

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertFalse(parser.role_main)
        self.assertFalse(parser.role_banner)
        self.assertFalse(parser.role_search)
        self.assertFalse(parser.role_nav)
        self.assertFalse(parser.role_contentinfo)

    def test_parser_finds_linked_image_link(self):
        """reports the link of a linked image"""
        img_src = (
            "http://www.zelda.com/"
            "breath-of-the-wild/assets/media/wallpapers/desktop-5.jpg"
        )

        link_href = "http://www.nintendo.com"

        test_html = """
        <html>
            <body>
                <a href="{0}">
                    <img src="{1}">
                </a>
            </body>
        </html>
        """.format(link_href, img_src)

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertEqual(len(parser.img_data), 1)
        self.assertEqual(parser.img_data[0]['link'], link_href)

    def test_parser_finds_linked_image_text(self):
        """reports the text of a linked image"""
        img_src = (
            "http://www.zelda.com/"
            "breath-of-the-wild/assets/media/wallpapers/desktop-5.jpg"
        )

        link_href = "http://www.nintendo.com"

        test_html = """
        <html>
            <body>
                <a href="{0}">
                    ZELDA
                    <img src="{1}">
                    <span>SO COOL</span>
                </a>
            </body>
        </html>
        """.format(link_href, img_src)

        parser = AccessibilityParser()
        parser.feed(test_html)

        self.assertEqual(len(parser.img_data), 1)
        self.assertEqual(parser.img_data[0]['link_text'], "ZELDA | SO COOL")
