# Introduction #

Hello! This README will help you set up your computer so the scripts in this
folder will run correctly. Don't worry, it will just be a bunch of copying and
pasting on your part. Maybe some reading comprehension. Let's get started!

# Installing Dependencies #
## Installing with setup.sh ##

A script exists in this repository that will install all of the dependencies
for you. To invoke it, you might need to do this first:

    chmod +x setup.sh

But after that, you can invoke it with:

    ./setup.sh

If you don't want to trust the setup script, the step-by-step instructions
are below. Each of these steps are performed automatically by setup.sh.

## Installing Manually ##
### Homebrew ###

Homebrew is a command-line based application search/get program for Mac OSX.
To install it, copy the following command in a Terminal window and press enter:

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

It'll ask for your password, and then it'll tell you all the things it's doing.

### axe-cli ###

Node is a javascript package manager, and we'll need it to install the aXe
command-line tools. These are optional tools for running the Accessibility
Report script.

    brew install node

Once that's done, you can get the aXe command-line tool by running this:

    npm install axe-cli -g

### GraphicsMagick ###

Screenshot comparisons are performed with the GraphicsMagick library, so you'll
need to grab that. Luckily, the complicated nonsense that used to be necessary
is handled easily by homebrew:

    brew install graphicsmagick

Simple!

### Python3 + Pip3 ###

Python3 comes by default on your Mac, but it has some weird permissions. You can
use Homebrew to install Python3 + Pip better by doing these commands:

    brew install python3
    brew link python --force

There, much better.

### Python3 modules ###

These scripts use a few Python3 modules. Copy/paste this command to get the
ones we need:

    pip3 install -r requirements.txt --user

### Drivers ###

Selenium needs `chromedriver` and `geckodriver` to be able to connect to
Google Chrome and Firefox, respectively. Grab those guys by running these:

    brew install chromedriver
    brew install geckodriver

# Using the Scripts #

You can invoke each script by using ./script_name.py or python script_name.py
and passing in the required arguments. Just doing the above will print out the
help for the script, which will explain the various inputs/options you can use
with it. Here are the defaults each script supports:

    "-u", "--username" : allows you to specify a username for BASIC or DIGEST authentication.
    "-p", "--password" : indicates that a password is needed - the script will prompt you before it starts.
    "-d", "--domain"   : the domain of the site where authentication is needed, such as "www.nintendo.com"
    "--basic"          : flags the auth for BASIC authentication.
    "--digest"         : flags the auth for DIGEST authentication.
    "--js"             : indicates that webdriver's browser must be used. This does not function for all the scripts.
    "--login"          : causes the script to pause before it starts, giving you time to log in.
    "-o", "--output"   : allows you to specify the output directory or name of the files.
    "--textme"         : allows you to pass in a phone number, so the script will send you a text message when it completes.

Beyond the defaults, each script has its own unique options which allow you to
craft the results into what you need. Use each script's `-h` or `--help` option
to find out more about that specific script.

# Maintaining the Scripts #

Most of the work is done in the various `common/` files. There are several
files to this end:

+ `authentication.py`
    + For any authentication functionality
+ `communications.py`
    + Handles texting, e-mails, etc.
+ `delegation.py`
    + Common work offloading
+ `exceptions.py`
    + Stores the custom exceptions these scripts use
+ `files.py`
    + Filename string manipulation, url-or-file delineation, etc.
+ `internet.py`
    + Any internet transactions (requests, responses, etc.)
+ `logging.py`
    + Custom logging for the scripts
+ `parsers.py`
    + The custom extensions of htmlparser
+ `urls.py`
    + URL string manipulation

# Help #

There is a wiki page that may or may not be updated as frequently as this file.

    http://qa.nerderylabs.com/index.php/Accessibility_Scripts

If you need any help installing or using these scripts, please reach out to me!
I'd be glad to help you get familiar with these.

    pgoy@nerdery.com
