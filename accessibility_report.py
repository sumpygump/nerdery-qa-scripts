#!/usr/local/bin/python3


"""
Version 4.0.0, written by Perry Goy at The Nerdery
"""

import argparse
import csv
import os
import subprocess
import sys
from urllib.parse import urljoin

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from jinja2 import Environment, FileSystemLoader, select_autoescape

from common import authentication
from common import communication
from common import delegation
from common import files
from common import internet
from common import parsers
from common import strings
from common.parsers import MAIN, BANNER, SEARCH, NAVIGATION, CONTENTINFO


parser = argparse.ArgumentParser(
    description=(
        "This script will check each URL provided for several accessibility "
        "requirements, all the images at the URL, and return an HTML report "
        "cataloguing all the tests performed, the results, the site's images "
        "and their alt texts."
    )
)
parser.add_argument(
    "url_file",
    help=(
        "the URL or URL file to use. Enter 'stepped' to have the report "
        "provide a browser for you to manually visit pages and add them to "
        "the report."
    )
)
parser.add_argument(
    "--focustest", action='store_true',
    help=(
        "perform a focus test. This will cause the script to use a "
        "WebDriver and load the page(s) in an actual browser (could cause "
        "the script to run more slowly)."
    )
)
parser.add_argument(
    "--bundle",
    help=(
        "bundle reports such that at most X many websites will be in each "
        "bundle. If not set, the bundle size will be 25. For example, "
        "`--bundle 5` will create bundles of 5 reports until all urls in "
        "url_file have been reported."
    ),
    default=25
)
parser.add_argument(
    "--axe", action='store_true',
    help=(
        "include the aXe command-line tool tests in the report. This will "
        "cause the script to run significantly more slowly. These results "
        "should be taken with a grain of salt -- the aXe tool is known to "
        "give frequent false-positives, and the command-line tool's results "
        "may differ from the browser plugin's results."
    )
)
parser.add_argument(
    "--nojs", action='store_true',
    help=(
        "run the script without launching a Selenium browser. The default "
        "behavior for this script is to use a browser to test against the "
        "site as a user would see it -- in a browser, with javascript.")
)
parser.add_argument(
    "--csv", action='store_true',
    help=(
        "ask the script to generate a CSV report of the image results in "
        "addition to the HTML report."
    )
)
delegation.add_defaults_to_parser(
    parser,
    ['u', 'p', 'basic', 'digest', 'js', 'login', 'wait',
     'height', 'width', 'o', 'textme']
)


def get_warning_class_list(parser):
    """
    Returns a list of class names to add to this report, based on all the
    warnings that fail.

    :param parser: the AccessibilityParser instance used to parse the
        website.
    :return: the list of strings of classes to add to the report.
    """
    warnings = {
        "main": parser.role_main,
        "language": parser.role_language,
        "banner": parser.role_banner,
        "search": parser.role_search,
        "nav": parser.role_nav,
        "contentinfo": parser.role_contentinfo,
        "dupIds": parser.ids_ok,
        "dupAttrs": parser.no_duped_attrs,
        "unclosedTags": parser.tags_closed,
        "badTabindex": parser.bad_tabindexes,
        "badIframes": len(parser.bad_iframes) == 0,
        "badMap": parser.image_ismap_ok,
        "tagsOutOfAria": (
            not (parser.role_main and not parser.out_of_landmarks)
        ),
    }

    class_list = []

    for name, ok in warnings.items():
        if not ok:
            class_list.append(name)

    return class_list


def create_warning_info(parser, add_reds=[], add_yellows=[]):
    """
    Creates the warnings that appear above each table, if applicable.

    :param parser: the AccessibilityParser instance used to parse the
        website.
    :param add_reds: a list of additional red warnings to add.
    :param add_yellows: a list of additional yellow warnings to add.
    :return: a dict of the red and yellow warnings (which are themselves
        lists of dicts)
    """
    red_warnings = add_reds + [
        {
            'ok': parser.ids_ok,
            'skipped': False,
            'fail_msg': "The following IDs were duplicated: <c>{0}</c>".format(
                ', '.join(parser.get_duped_ids())),
            'pass_msg': "No duplicate IDs found.",
        },
        {
            'ok': parser.role_language,
            'skipped': False,
            'fail_msg': "No <c>lang</c> attribute found on this page!",
            'pass_msg': "<c>lang</c> attribute present.",
        },
        {
            'ok': parser.tags_closed,
            'skipped': False,
            'fail_msg': (
                "The following tags were not closed: <c>{0}</c>".format(
                    ', '.join(parser.strip_void_elements(parser.tag_queue))
                )
            ),
            'pass_msg': "All tags were properly closed.",
        },
        {
            'ok': parser.no_duped_attrs,
            'skipped': False,
            'fail_msg': (
                "These tags had duplicated attributes: <c>{0}</c>".format(
                    parser.get_duped_attr_string()
                )
            ),
            'pass_msg': "No tags had duplicate attributes.",
        },
        {
            'ok': not parser.bad_tabindexes,
            'skipped': False,
            'fail_msg': (
                "The following incorrect tabindexes appeared: " +
                ', '.join(sorted(parser.bad_tabindexes))
            ),
            'pass_msg': (
                "All <c>tabindex</c> attributes are less than or equal to 0."
            ),
        },
        {
            'ok': parser.image_ismap_ok,
            'skipped': False,
            'fail_msg':
                "One or more images had <c>ismap</c> but no <c>usemap</c>!",
            'pass_msg':
                "All images (if any) correctly used <c>ismap/usemap</c>.",
        },
        {
            'ok': len(parser.bad_iframes) == 0,
            'skipped': False,
            'fail_msg': (
                str(len(parser.bad_iframes)) + " <c>iframe</c> element" +
                ("s " if len(parser.bad_iframes) != 1 else " ") +
                "did not have a title!"
            ),
            'pass_msg': "All <c>iframe</c> elements (if any) had titles.",
        },
    ]

    yellow_warnings = add_yellows + [
        {
            'ok': parser.role_main,
            'skipped': False,
            'fail_msg':
                "No <c>role='{0}'</c> found on this page!".format(MAIN),
            'pass_msg': "<c>role='{0}'</c> was present.".format(MAIN),
        },
        {
            'ok': parser.role_banner,
            'skipped': False,
            'fail_msg':
                "No <c>role='{0}'</c> found on this page!".format(BANNER),
            'pass_msg': "<c>role='{0}'</c> was present.".format(BANNER),
        },
        {
            'ok': parser.role_search,
            'skipped': False,
            'fail_msg':
                "No <c>role='{0}'</c> found on this page!".format(SEARCH),
            'pass_msg': "<c>role='{0}'</c> was present.".format(SEARCH),
        },
        {
            'ok': parser.role_nav,
            'skipped': False,
            'fail_msg':
                "No <c>role='{0}'</c> found on this page!".format(NAVIGATION),
            'pass_msg': "<c>role='{0}'</c> was present.".format(NAVIGATION),
        },
        {
            'ok': parser.role_contentinfo,
            'skipped': False,
            'fail_msg':
                "No <c>role='{0}'</c> found on this page!".format(CONTENTINFO),
            'pass_msg': "<c>role='{0}'</c> was present.".format(CONTENTINFO),
        },
        {
            'ok': not parser.spelling_errors,
            'skipped': False,
            'fail_msg': (
                "One or more images had incorrectly spelled words in their "
                "alt or desc attributes. See the table below."
            ),
            'pass_msg': (
                "All images had correctly spelled text in their alt/desc "
                "attributes."
            ),
        },
        {
            'ok': not (parser.role_main and parser.out_of_landmarks),
            'skipped': not parser.role_main,
            'fail_msg': "Content tags appeared outside of ARIA landmarks!",
            'pass_msg': "All tags were contained in ARIA landmarks.",
            'skipped_msg': (
                "No ARIA role 'main' was found; testing for content tags out "
                "of landmarks was skipped."
            ),
        },
    ]

    return {
        'red': red_warnings,
        'yellow': yellow_warnings,
    }


def get_image_classes(image):
    """
    Creates the image class list based on the items in `image`.

    :param image: the img/svg object from the AccessibilityParser.
    :return: the list of classes this image should have.
    """
    classes = []

    for key, value in image['attrs'].items():
        if '<em>' not in value and '<strong>' not in value:
            # Default message with <em> or <strong> tag was overwritten
            classes.append(key)
        else:
            if key == "alt" and "no link text" in value:
                classes.append("no_link_text")
            elif key == "alt" and "skipped" in value:
                classes.append("image_skip")
            # Default message was not overwritten
            classes.append("no_" + key)

    if len(image['misspelled_words']) > 0:
        classes.append("no_spelling_ok")
    else:
        classes.append("spelling_ok")

    return classes


def get_image_data(parser, url, image_data):
    """
    Standardizes the given image_data, preparing it for use by the
    HTML templater.

    :param image_data: the img or svg data from AccessibilityParser.
    :param url: the url that was parsed.
    :return: the img data organized into a list of dicts.
    """
    images = list()

    for img_or_svg in image_data:
        img_or_svg['class_str'] = ' '.join(get_image_classes(img_or_svg))

        image_attrs = img_or_svg['attrs']

        if 'src' in image_attrs.keys() and image_attrs['src'] != '':
            img_or_svg['attrs']['src'] = urljoin(url, image_attrs['src'])

        if "<em>" not in img_or_svg['link']:
            img_or_svg['link'] = strings.escape_braces(img_or_svg['link'])

            if img_or_svg['link_text'] != '':
                img_or_svg['link_text'] = strings.escape_braces(
                    img_or_svg['link_text']
                )

        if (
            'alt' in image_attrs and
            image_attrs['alt'] in ["", parser.ATTR_DEFAULT, parser.ALT_DEFAULT]
        ):
            alt_data = parser.ALT_DEFAULT

            if img_or_svg['link_text']:
                alt_data = "<em>Blank alt; link text should be read.</em>"
            elif img_or_svg['link'] != parser.LINK_DEFAULT:
                if image_attrs['alt'] == parser.ALT_DEFAULT:
                    alt_data = "<strong>Blank alt, no link text!</strong>"
            else:
                if image_attrs['alt'] != parser.ALT_DEFAULT:
                    alt_data = (
                        "<em>Blank alt; image should be skipped by "
                        "readers.</em>"
                    )

            img_or_svg['attrs']['alt'] = alt_data

        images.append(img_or_svg)

    return images


def perform_aXe_test(url):
    """
    Performs the aXe command-line tool's test against the given URL and
    returns the results of that test.

    :param url: the URL against which to run the aXe command-line tool.
    :return: the results of the aXe test.
    """
    results_process = subprocess.run(
        'axe {} --browser chrome'.format(url),
        shell=True,
        stdout=subprocess.PIPE
    )

    results_rough = results_process.stdout.decode('utf8')
    results_prepared = prepare_text_for_report(results_rough)
    results_final = ""

    for line in results_prepared.split("<br />"):
        if line.strip().startswith("Violation"):
            line = '<span class="axeViolation">{}</span>'.format(line)
        elif line.strip() == "0 violations found!":
            line = '<span class="axePass">{}</span>'.format(line)

        results_final += line + "<br />"

    return results_final


def prepare_text_for_report(txt):
    """
    Escapes all <s and >s by replacing them with their &-codes and then
    replaces any newlines with <br />.

    :param txt: the text string to fix up.
    :return: the fixed string.
    """
    s = txt.replace("<", "&lt;").replace(">", "&gt;").replace("\n", "<br />")

    return s


def get_images_class_list(images):
    """
    Gets a class list for all of the images in the array `images`.

    :param images: a list of img or svg data from the AccessibilityParser.
    :return: a list of classes for all of the images in `images`.
    """
    image_classes = set()

    for image in images:
        image_classes = image_classes.union(get_image_classes(image))

    return [
        class_ for class_ in image_classes if
        'no_' + class_ not in image_classes
    ]


def get_report_class_list(parser):
    """
    Gets the class list for this page's report.

    :param parser: the AccessibilityParser used to parse this page.
    :return: a list of class strings.
    """
    report_classes = get_warning_class_list(parser)
    image_classes = get_images_class_list(parser.img_data + parser.svg_data)

    return report_classes + list(image_classes)


def perform_focus_test(driver):
    """
    Finds all <a> tags with a "#" href, searches for the element with the ID
    that would gain focus if the <a> tag were clicked, then determines if
    that element is an element that should be focusable.

    :param driver: the WebDriver instance to use to perform the test.
    :return: a list of each href that did not focus a focusable element.
    """
    unfocusable = ["html", "fieldset", "legend", "label",
                   "main", "table", "td"]

    bad_anchors = []
    for ele in driver.find_elements_by_xpath("//a[starts-with(@href, '#')]"):
        anchor = ele.get_attribute("href").split("#")[-1]

        if not anchor:
            continue

        try:
            anchor_ele = driver.find_element_by_id(anchor)
        except NoSuchElementException:
            continue

        if anchor_ele.tag_name in unfocusable:
            bad_anchors.append(anchor)

    return bad_anchors


def get_accessibility_report(url, source, driver=None, include_axe=False):
    """
    Performs the accessibility tests and prepares a report.

    :param url: the URL to report on.
    :param include_axe: whether to include the aXe results.
    :return: a report dict.
    """
    report = dict()

    parser = parsers.AccessibilityParser()
    parser.feed(source)

    report_classes = get_report_class_list(parser)

    if driver:
        bad_anchors = perform_focus_test(driver)

        add_reds = [
            {
                'ok': not bad_anchors,
                'skipped': False,
                'fail_msg': (
                    "The following hrefs went to non-focusable elements: " +
                    "<code>" + ', '.join(bad_anchors) + "</code>"
                ),
                'pass_msg':
                    "Each anchor link correctly focused a focusable element."
            }
        ]

        if bad_anchors:
            report_classes.append('badAnchors')

    else:
        add_reds = [
            {
                'ok': True,
                'skipped': True,
                'skipped_msg': "Focus test was not performed.",
            }
        ]

    if include_axe:
        axe_results = perform_aXe_test(url)

    else:
        axe_results = None

    warnings = create_warning_info(parser, add_reds=add_reds)

    out_of_landmarks_data = [
        prepare_text_for_report(txt) for txt in parser.out_of_landmarks_data
    ]

    report['class_str'] = " ".join(report_classes)
    report['url'] = url
    report['title'] = parser.title
    report['language'] = parser.role_language
    report['red_warnings'] = warnings['red']
    report['yellow_warnings'] = warnings['yellow']
    report['include_out_of_landmarks_data'] = parser.role_main
    report['out_of_landmarks_data'] = out_of_landmarks_data
    report['include_bad_iframes_data'] = len(parser.bad_iframes) > 0
    report['bad_iframes_data'] = parser.bad_iframes
    report['axe_results'] = axe_results
    report['img_classes'] = " ".join(get_images_class_list(parser.img_data))
    report['svg_classes'] = " ".join(get_images_class_list(parser.svg_data))
    report['images'] = get_image_data(parser, url, parser.img_data)
    report['svgs'] = get_image_data(parser, url, parser.svg_data)

    return report


def get_stepped_report(driver, include_axe=False):
    """
    Allows the user to navigate manually, adding pages to their report as
    they go.

    :param driver: the WebDriver instance of the manual browser.
    :param include_axe: whether or not to include the aXe results.
    :return: a list of report dicts.
    """
    print("***")
    print("You are now creating a manual list of URLs to include\n"
          "in your accessibility report. Simply change the URL in\n"
          "the provided browser window and enter 'add' when you\n"
          "would like to add the current page to the report. When\n"
          "finished, enter 'done'.")
    print("***")
    inp = input("Waiting for first page... ")

    report_list = list()

    while inp.lower() != 'done':
        ready = driver.execute_script("return document.readyState")

        # Try to wait until the page is done loading
        while ready != 'complete':
            ready = driver.execute_script("return document.readyState")
            continue

        url_data = internet.get_source_with_driver(driver)

        if not internet.source_is_html(url_data['source']):
            # Skip pages that aren't HTML content, such as PDFs
            print(
                "** This page does not appear to have any HTML.\n"
                "** Please navigate to a different page before trying again!"
            )
            continue

        report_list.append(
            get_accessibility_report(
                url_data['url'],
                url_data['source'],
                driver=driver,
                include_axe=include_axe
            )
        )

        inp = input("Page added! Waiting for next page... ")
    print("OK! Finishing report...")

    return report_list


def generate_html_report_argparse(args):
    """
    A wrapper to handle the args passed in by ArgParse.

    :param args: the parsed args from ArgParse.
    :return: None
    """
    url_list = files.get_url_list(args.url_file)
    filename = args.output or "accessibility_report.html"
    auth = authentication.get_auth_dict(args)
    wait = float(args.wait) if args.wait else None

    # Default for this script is to run in a browser, contrary to others.
    args.js = not args.nojs

    driver = internet.get_driver_or_none(args, url_list[0])

    return generate_html_report(
        url_list,
        filename,
        auth=auth,
        wait=wait,
        driver=driver,
        include_axe=args.axe,
        bundle_max=int(args.bundle),
        create_csv=args.csv,
        textme=args.textme
    )


def generate_html_report(
    url_list,
    output_name="accessibility_report.html",
    auth=None,
    wait=None,
    driver=None,
    include_axe=False,
    bundle_max=25,
    create_csv=False,
    textme=None
):
    """
    Generates the HTML report using the information from
    the AccessibilityParser.

    :param url_list: the URL or list of urls to report on, or the word
        'stepped' to manually add pages to the report.
    :param output_name: the desired name of the output file(s).
    :param auth: an auth package -- a dict with username, password, and
        domain.
    :param wait: how many seconds to wait after each page finishes
        loading, if necessary. Default is 0.
    :param driver: the driver to use, or None if no driver is needed.
        Default is None.
    :param include_axe: whether to run the aXe command-line tool and
        include its results in the report. Default is False.
    :param bundle_max: how many pages to include in a single report.
        Default is 25.
    :param textme: the phone number to text after the report is finished.
        Default is None.
    :return: a list of all the reports.
    """
    bundle_num = 1
    bundled_urls = [
        url_list[x:(x + bundle_max)]
        for x in range(0, len(url_list), bundle_max)
    ]

    script_dir = os.path.dirname(os.path.realpath(__file__))

    jinja2_env = Environment(
        loader=FileSystemLoader(script_dir + '/common/templates'),
        autoescape=select_autoescape(['html'])
    )

    report_template = jinja2_env.get_template(
        "accessibility_report_template.html"
    )

    full_report_list = list()

    for bundle in bundled_urls:
        report_bundle = list()

        if bundle_max < len(url_list):
            split_name = output_name.split(".")

            try:
                split_name[-2] = "{0}_bundle{1}-{2}".format(
                    split_name[-2],
                    (bundle_num - 1) * bundle_max + 1,
                    min(bundle_num * bundle_max, len(url_list))
                )
            except IndexError:
                msg = "Please provide an output filename that ends in '.html'."
                print(msg)
                sys.exit(1)

            filename = ".".join(split_name)
        else:
            filename = output_name

        if len(bundle) == 1 and bundle[0] == 'stepped':
            report_bundle = get_stepped_report(driver, include_axe=include_axe)

        else:
            for url in bundle:
                if url == '':
                    continue

                if driver:
                    try:
                        driver.get(url)
                        url_data = internet.get_source_with_driver(driver)
                    except TimeoutException:
                        msg = "** Skipping " + url + " -- timeout error."
                        print(msg)
                        continue
                else:
                    url_data = internet.get_source_with_requests(url, auth)

                # Skip over pages that aren't HTML content, such as PDFs
                if not internet.source_is_html(url_data['source']):
                    msg = (
                        "** Skipping " + url_data['url'] +
                        " -- does not appear to be HTML."
                    )
                    print(msg)
                    continue

                report_bundle.append(
                    get_accessibility_report(
                        url_data['url'],
                        url_data['source'],
                        driver=driver,
                        include_axe=include_axe
                    )
                )

        with open(filename, 'w+') as html_report:
            html_report.write(report_template.render(reports=report_bundle))

        full_report_list.extend(report_bundle)

        bundle_num += 1

    if driver:
        driver.quit()

    completed_message = "Accessibility report complete! Find results in {0}"
    completed_message = completed_message.format(output_name)

    if bundle_max < len(url_list):
        completed_message += " (in {0} bundles)".format(
            int(len(url_list) / bundle_max))

    if create_csv:
        for report in full_report_list:
            filename = output_name.split(".")[0] + ".csv"
            with open(filename, 'a+') as csv_file:
                writer = csv.writer(csv_file)

                # Headers
                writer.writerow(
                    ['url', 'src', 'alt', 'link text', 'link', 'filters']
                )

                for image in report['images']:
                    writer.writerow(
                        [
                            report['url'],
                            image['src'],
                            image['alt'],
                            image['link_text'],
                            image['link'],
                            image['class_str'],
                        ]
                    )
        completed_message += " and {0}".format(filename)

    if textme:
        communication.send_text(textme, completed_message)

    print(completed_message)

    return full_report_list


if __name__ == "__main__":
    if len(sys.argv) > 1:
        args = parser.parse_args()
        generate_html_report_argparse(args)
    else:
        parser.print_help()
else:
    print("This module is meant to be used from the commandline.")
