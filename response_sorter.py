#!/usr/local/bin/python3


import argparse
import collections
import sys

from common import authentication
from common import communication
from common import delegation
from common import files
from common import internet


parser = argparse.ArgumentParser(
    description=(
        "Using a file of URLs or a single URL, sends a HEAD request to each "
        "URL and sorts the responses received into text files based on status "
        "code, keeping track of any redirects encountered."
    )
)
parser.add_argument(
    "url_file",
    help="the file containing all URLs to check."
)
parser.add_argument(
    "--nofollow", action="store_false",
    help=("indicate the script should not follow redirects. By default, the "
          "script will follow any 30x redirects to their conclusion.")
)
delegation.add_defaults_to_parser(
    parser, ['u', 'p', 'basic', 'digest', 'login', 'threads', 'o', 'textme']
)


def sort_responses(args):
    """
    Uses requests' 'head' method to ping each URL in the provided URL list
    and gathers the response codes from each. Spits out the results into
    files named after the response codes received.

    :param args: the dictionary of parsed arguments from argparse.
    """
    urls = files.get_url_list(args.url_file)
    auth = authentication.get_auth_dict(args)
    file = args.output or "response_codes.txt"
    map_file = files.get_filename(file) + "_redirect_map.txt"

    if args.login:
        header = authentication.wait_for_login_with_requests()
    else:
        header = {}

    responses = delegation.get_with_threaded_map(
        lambda url: internet.get_response_safely(
            url.strip(),
            auth,
            header,
            args.nofollow
        ),
        urls,
        num_threads=int(args.threads))
    response_map = collections.defaultdict(set)
    redirect_map = []

    for origin, url, code in responses:
        response_map[code].update([url])
        if origin != url:
            redirect_map.append((origin, url))

    for code in list(response_map.keys()):
        codegroup_file = "{code_num}_{filename}".format(
            code_num=code,
            filename=file)
        with open(codegroup_file, 'w+') as output:
            for url in sorted(response_map[code]):
                output.write(url + '\n')

    with open(map_file, 'w+') as map_output:
        for origin, url in redirect_map:
            map_output.write("{0} > {1}\n".format(origin, url))

    completed_message = "Response sort complete!"
    completed_message += " Outputs can be found in files ending with '{0}'. "
    completed_message = completed_message.format(file)
    if len(redirect_map) > 0:
        completed_message += "(And the redirect map can be found in '{0}')"
        completed_message = completed_message.format(map_file)

    if args.textme:
        communication.send_text(args.textme, completed_message)

    print(completed_message)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        args = parser.parse_args()
        sort_responses(args)
    else:
        parser.print_help()
else:
    print("This module is meant to be used as a standalone script.")
