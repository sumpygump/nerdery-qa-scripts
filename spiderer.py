#!/usr/local/bin/python3


"""
Version 4.2.0, written by Perry Goy at The Nerdery
"""


import argparse
import os
from pprint import pprint
import sys

from common import authentication
from common import communication
from common import delegation
from common import internet
from common import files
from common import parsers
from common import urls


parser = argparse.ArgumentParser(
    description=(
        "Visits a URL and scrapes all the links off of the page, then visits "
        "each of those links and does the same, visits all of those links... "
        "then returns a full list of all URLs on that domain which can be "
        "reached this way. Reports all the URLs alphabetically in a text file."
    )
)
parser.add_argument(
    "url",
    help="the base URL, the starting point for the spider."
)
parser.add_argument(
    "--seed",
    help="a file containing a list of URLs to prime the 'visited' list with.",
)
parser.add_argument(
    "--depth",
    help=("how many links away from the base url the spider should click. "
          "Default is no limit."),
    default=sys.maxsize
)
parser.add_argument(
    "-a", "--all", action='store_true',
    help=(
        "report ALL the URLs found on pages in the domain, not just the "
        "URLs within the domain."
    )
)
parser.add_argument(
    "--map", action='store_true',
    help=(
        "indicates that the output should show which pages each URL came "
        "from. Creates an additional output file which shows each page's URL "
        "and what URLs were on that page."
    )
)
parser.add_argument(
    "-e", "--exclude",
    help=(
        "a comma separated list of additional flags in the url for the spider "
        "to skip. For example, add 'calendar' to the URL to skip all URLs "
        "with the word 'calendar' in them."
    )
)
parser.add_argument(
    "-r", "--restrict_url",
    help=(
        "specify a url that must be contained in all urls the spider visits "
        "and reports."
    )
)
delegation.add_defaults_to_parser(
    parser,
    ['u', 'p', 'basic', 'digest', 'threads', 'js',
     'login', 'wait', 'o', 'textme'])


def scrape_links(url_info):
    """
    Scrapes all the links off the given page.

    Arguments:
        url_info - a pair of [url, page source], both as strings.
    """
    origin = url_info['url']
    source = url_info['source']

    if source is None or not internet.source_is_html(source):
        links = []

    else:
        link_parser = parsers.LinkParser(origin)
        link_parser.feed(source)

        links = link_parser.links

    return (origin, links)


def spider_argparse(args):
    """
    A wrapper to extract the required parameters from argparse and pass
    them into spiderer.

    :param args: the dictionary of parsed arguments from argparse.
    """
    return spider(
        args.url,
        seed=files.get_url_list(args.seed),
        auth=authentication.get_auth_dict(args),
        output=args.output or "spider_report.txt",
        depth=int(args.depth),
        restrict_url=args.restrict_url,
        also_exclude=args.exclude.split(",") if args.exclude else [],
        report_all=args.all,
        driver=internet.get_driver_or_none(args, args.url),
        wait=int(args.wait) if args.wait else 0,
        include_map=args.map,
        threads=int(args.threads),
        textme=args.textme
    )


def spider(
    url, seed=[], auth=None, output="spider_report.txt", depth=sys.maxsize,
    restrict_url="", also_exclude=[], report_all=False, driver=None, wait=0,
    include_map=False, threads=4, textme=None
):
    """
    Performs a spider on the given base_url, scraping all links from the
    base_url page, then going to each of those links (provided they are
    on the same domain) and repeating that process.

    :param url: the URL to start the spider at.
    :param seed: the list of URLs to prime visited_urls with.
    :param auth: an auth pack to access the site, if necessary.
        Default is None.
    :param output: the desired name of the output file.
        Default is "spider_report.txt".
    :param depth: how many clicks away from `url` to go.
        Default is sys.maxsize (no limit).
    :param restrict_url: specify a substring that *must* appear in each
        URL that is visited. Default is "".
    :param also_exclude: a list of additional exclusion strings.
        Default is [].
    :param report_all: whether or not to report *all* seen URLs, not just
        the ones that fit the exclusions and domain restrictions.
        Default is False.
    :param driver: the driver to use, if needed. Default is None.
    :param wait: how long to wait after the page loads (will be ignored if
        `driver` is None). Default is 0.
    :param include_map: whether or not to generate a map of the URLs.
        Default is False.
    :param threads: how many threads to use. Default is 4.
    :param textme: if a phone number (as a string) is provided, Spiderer
        will text you when the results are complete. Default is None.
    """
    base = urls.get_base(url)

    partial_file = "partial_" + output

    visited_urls = set(seed)
    url_map = dict()
    new_urls = set([base['original']])
    cur_depth = 0

    print(visited_urls)

    if include_map:
        url_map = {base['original']: set()}

    print("spiderer.py: Finding links (this may take a while)...")
    while len(new_urls) > 0:
        if cur_depth > depth:
            visited_urls.update(new_urls)
            break

        status_msg = "Depth: {0: <5} Number of URLs to visit: {1}"
        print(status_msg.format("{},".format(cur_depth), len(new_urls)))

        page_sources = []

        if driver:
            for url in new_urls:
                page_sources.append(
                    internet.get_source_with_driver(
                        driver,
                        url,
                        wait=wait
                    )
                )
        else:
            page_sources = delegation.get_with_threaded_map(
                lambda url: internet.get_source_with_requests(url, auth),
                new_urls,
                num_threads=threads
            )

        updated_urls = set([x['url'] for x in page_sources])
        visited_urls.update(new_urls.union(updated_urls))

        with open(partial_file, 'a+') as concurrent_report:
            for url in updated_urls:
                concurrent_report.write(url + "\n")

        new_urls = set()
        cur_depth += 1

        scraped_links = [scrape_links(url_info) for url_info in page_sources]

        add_to_new = []
        for origin, links in scraped_links:
            if include_map:
                if origin in url_map.keys():
                    url_map[origin].update(links)

                else:
                    url_map[origin] = set(links)

            pruned_links = urls.apply_exclusions(
                base, origin, visited_urls, links,
                add_exclude=also_exclude,
                restrict_url=restrict_url,
                report_all=report_all
            )

            add_to_new.append(pruned_links)

        # The map created a list for each page source, so it's a list of
        # lists. We need to flatten that.
        flat_links = [url_lst for lst in add_to_new for url_lst in lst]

        for link in flat_links:
            new_urls.add(link)

    if driver:
        driver.quit()

    with open(output, "w+") as log:
        for url in sorted(visited_urls):
            log.write(url + "\n")

    os.remove(partial_file)

    msg = "Spider of {0} is done! Discovered {1:,} URLs. Find results in {2}"
    msg = msg.format(base['original'], len(visited_urls), output)

    if include_map:
        mapfile = files.get_filename(output) + '_map.txt'

        msg += " and in {0}".format(mapfile)

        with open(mapfile, 'w+') as map_log:
            pprint(url_map, map_log)

    if args.textme:
        communication.send_text(args.textme, msg)

    print(msg)

    return {
        'url_list': list(visited_urls),
        'map': {x: list(y) for x, y in url_map.items()}
    }


if __name__ == "__main__":
    if len(sys.argv) > 1:
        args = parser.parse_args()

        if not args.url.startswith("http"):
            err_msg = "Base URL must begin with http:// or https://."
            err_msg = "Please try again!"
            sys.exit(1)

        spider_argparse(args)
    else:
        parser.print_help()
else:
    print("This module is meant to be used as a standalone script.")
