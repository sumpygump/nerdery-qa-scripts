from urllib.parse import urljoin
from urllib.parse import urlparse


def apply_exclusions(base, origin, visited_urls, url_list,
                     add_exclude=[], restrict_url="", report_all=False):
    """
    Applies the exclusion rules to the given list and returns the pruned
    list, fixing URLs as they need to be fixed.

    :param base: the base object created by get_base().
    :param origin: the link from which the urls in url_list were scraped.
    :param visited_urls: the full list of urls already visited.
    :param url_list: the list of URLs to prune.
    :param add_exclude: a list of additional exclusion strings.
        Default is [].
    :param restrict_url: a string that must appear in each URL.
        Default is "".
    :param report_all: whether or not to report *all* the URLs found.
        Default is False.
    :return: the list of pruned URLs.
    """
    end_exc_list = [
        '.png', '.jpg', '.pdf', '.gif', '.mpg', '.mkv', '.mov', '.avi', '.css',
        '.js', '.ashx', '.zip',
    ]
    start_exc_list = ['#', 'mailto:', 'tel:', 'javascript:', ]

    pruned_urls = []

    for url in url_list:
        if url is None or url == "":
            continue

        no_starts = all(
            [not url.lower().startswith(x) for x in start_exc_list]
        )

        no_ends = all([not url.lower().endswith(x) for x in end_exc_list])

        url = urljoin(origin, url)
        new_base = get_base(url)

        if url not in visited_urls:
            if restrict_url and restrict_url not in url:
                continue

            no_addtl = all(
                [x.lower() not in url.lower() for x in add_exclude]
            )

            should_include = no_ends and no_starts and no_addtl

            if should_include and base['domain'] in new_base['domain']:
                pruned_urls.append(url)

            if report_all:
                visited_urls.add(url)

    return pruned_urls


def get_base(url):
    """
    Returns info about the base URL contained in a url.

    :param url: the URL to break up.
    :return: A map containing information about the URL.
    """
    parsed_url = urlparse(url)

    base = {}
    base['original'] = url
    base['protocol'] = parsed_url.scheme

    netloc_split = parsed_url.netloc.split('.')
    if len(netloc_split) > 2:
        base['subdomain'] = netloc_split[0]
        base['domain'] = '.'.join(netloc_split[1:])
    else:
        base['subdomain'] = "www"
        base['domain'] = parsed_url.netloc

    base['path'] = parsed_url.path
    if parsed_url.query:
        base['path'] += "?" + parsed_url.query
    if parsed_url.fragment:
        base['path'] += "#" + parsed_url.fragment

    base['url'] = "{0}://{1}.{2}/".format(
        base['protocol'],
        base['subdomain'],
        base['domain'])

    return base
