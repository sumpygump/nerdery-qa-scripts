from multiprocessing.dummy import Pool


def add_defaults_to_parser(parser, desired_args):
    """
    Creates the default argument parser which contains all the arguments
    supported by all the scripts.

    :param parser: the initialized ArgParser object.
    :param desired_args: the flags to add to the parser (without the -s).
    :return: The passed-in parser, updated in-place.
    """
    args = {
        "u": ["-u", "--username"],
        "p": ["-p", "--password"],
        "basic": ["--basic"],
        "digest": ["--digest"],
        "threads": ["--threads"],
        "js": ["--js"],
        "login": ["--login"],
        "wait": ["--wait"],
        "height": ["--height"],
        "width": ["--width"],
        "o": ["-o", "--output"],
        "textme": ["--textme"],
    }

    kwargs = {
        "u": {
            "help": "username for auth, if required."
        },
        "p": {
            "action": 'store_true',
            "help": ("indicates a password is needed. You will be "
                     "prompted to enter it.")
        },
        "basic": {
            "action": 'store_true',
            "help": ("indicates Basic authentication should be used. "
                     "This is the assumed authentication if neither "
                     "--basic nor --digest was supplied.")
        },
        "digest": {
            "action": 'store_true',
            "help": "indicates Digest authentication should be used."
        },
        "threads": {
            "default": 4,
            "help": ("how many threads to use for cURLing. Default is 4; "
                     "probably shouldn't exceed 10."),
        },
        "js": {
            "action": 'store_true',
            "help": ("indicate that javascript is required. This is "
                     "needed for pages that do not fully populate "
                     "the page source when javascript is disabled. "
                     "This causes the script to use a browser window "
                     "instead of requests/urllib to get sources.")
        },
        "login": {
            "action": 'store_true',
            "help": ("indicate that you will need to log in. This "
                     "causes the script to use a browser window "
                     "instead of requests/urllib to get sources.")
        },
        "wait": {
            "help": ("specify a wait time, to wait for the page to "
                     "load after the page has... loaded. Useful for "
                     "pages that have elements which are built with "
                     "javascript.")
        },
        "height": {
            "default": '1177',
            "help": ("specify the height for the browser window (if "
                     "the script will be using a browser). Default "
                     "is 1177.")
        },
        "width": {
            "default": '1080',
            "help": ("specify the width for the browser window (if "
                     "the script will be using a browser). Default "
                     "is 1080.")
        },
        "o": {
            "help": "the preferred name of the output file."
        },
        "textme": {
            "help": "Send a text to this number when the script has finished."
        },
    }

    for flag in desired_args:
        parser.add_argument(*args[flag], **kwargs[flag])

    return parser


def get_with_threaded_map(func, collection, num_threads=4):
    """
    Performs the given task with a number of threads.

    :param func: the function to apply to the collection.
    :param collection: the thing to map over.
    :param num_threads: the number of threads to use to perform the task.
                        Default is 4.
    :return: the modified collection.
    """
    workpool = Pool(num_threads)
    threaded_map = workpool.map_async(func, collection)
    workpool.close()
    workpool.join()

    return threaded_map.get()
