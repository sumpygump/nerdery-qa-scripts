class NoMatchFoundError(Exception):
    """
    A custom exception to handle when regex matches fail to match.
    """
    pass
