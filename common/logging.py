from datetime import datetime


LOGFILE = "common.log"


def get_logtime():
    """
    Gets the current time, to add to logs.

    :return: a string representing the current date, as YYYY-MMM-DD.
    """
    return datetime.now().strftime("%Y-%b-%d")


def log(msg):
    """
    Logs the given message to the logfile!

    :param msg: the message to log.
    """
    with open(LOGFILE, 'a+') as log:
        log.write(msg)
