from html.parser import HTMLParser
import urllib.request
import urllib.parse
import urllib.error

import enchant

from common import strings


MAIN = 'main'
BANNER = 'banner'
SEARCH = 'search'
NAVIGATION = 'navigation'
CONTENTINFO = 'contentinfo'


class LinkParser(HTMLParser):
    """
    An extension of HTMLParser to find all the linked URLs on a page.
    """
    def __init__(self, origin):
        HTMLParser.__init__(self)
        self.links = []
        self.origin = origin

    def handle_starttag(self, tag, attributes):
        if tag != 'a':
            return

        for name, value in attributes:
            if name == 'href' and value is not None:
                value = value.strip()
                safe_chars = ":/~#?=&%"
                try:
                    safe_url = urllib.parse.quote(value, safe=safe_chars)
                    self.links.append(safe_url)
                except Exception as e:
                    exc_msg = "{0}: {1}".format(type(e).__name__, e)
                    print("Error parsing value '{0}' on {1} -- {2}".format(
                        value, self.origin, exc_msg))
        else:
            return

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        pass


class ObjectParser(HTMLParser):
    """
    An extension of HTMLParser to check for specific objects.
    """
    def __init__(self, tagname, pairs, content, partial, invert):
        HTMLParser.__init__(self)
        self.tagname = tagname
        self.pairs = pairs
        self.content = content
        self.partial = partial
        self.bad_objects = []
        self.invert = invert
        self.presence = False
        self.check_content = False

    def handle_starttag(self, tag, attributes):
        if tag != self.tagname:
            return

        if attributes:
            html_attrs = ' '.join(
                ['{0}="{1}"'.format(x[0], x[1]) for x in attributes])
        else:
            html_attrs = ''

        if self.pairs:
            pairs_cpy = list(self.pairs)

            if self.partial:
                for name, value in attributes:
                    for pair_name, pair_value in pairs_cpy:
                        if pair_name != name:
                            continue

                        if pair_value in value:
                            pairs_cpy.remove([pair_name, pair_value])

                if pairs_cpy and self.content:
                    self.check_content = True
                    self.tag_attrs = html_attrs
            else:
                for name, value in attributes:
                    try:
                        pairs_cpy.remove([name, value])
                    except ValueError:
                        pass

            if self.invert:
                if pairs_cpy:
                    if self.content:
                        self.check_content = True
                        self.tag_attrs = html_attrs
                    else:
                        self.bad_objects.append(html_attrs)
            else:
                if not pairs_cpy:
                    if self.content:
                        self.check_content = True
                        self.tag_attrs = html_attrs
                    else:
                        self.bad_objects.append(html_attrs)
        elif self.content:
            self.check_content = True
            self.tag_attrs = html_attrs
        else:
            self.bad_objects.append(html_attrs)

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        if self.check_content:
            obj = '<{tag} {attrs}>{content}</{tag}>'.format(
                tag=self.tagname,
                attrs=self.tag_attrs,
                content=data
            )

            if (
                (
                    self.invert and
                    self.partial and
                    not any([thing in data for thing in self.content])
                ) or (
                    not self.invert and
                    self.partial and
                    all([thing in data for thing in self.content])
                ) or (
                    self.invert and
                    not self.partial and
                    not any([thing == data.strip() for thing in self.content])
                ) or (
                    not self.invert and
                    not self.partial and
                    all([thing == data.strip() for thing in self.content])
                )
            ):
                self.bad_objects.append(obj)

            self.check_content = False
            self.tag_attrs = None


class AccessibilityParser(HTMLParser):
    """
    An extension of HTMLParser to check for accessibility requirements.
    """
    def __init__(self):
        HTMLParser.__init__(self)

        # Spellchecking
        self.dict = enchant.Dict("en_US")

        # Defaults
        self.LINK_DEFAULT = '<em>Not linked.</em>'
        self.ALT_DEFAULT = '<strong>No alt!</strong>'
        self.SRC_DEFAULT = '<em>No src.</em>'
        self.ATTR_DEFAULT = '<em>Attribute present, no value specified.</em>'
        self.TITLE_DEFAULT = '<em>No title.</em>'

        # Important accessibility data
        self.title = ''
        self.ids_ok = True
        self.bad_iframes = []
        self.role_language = False
        self.role_main = False
        self.role_banner = False
        self.role_search = False
        self.role_nav = False
        self.role_contentinfo = False
        self.bad_tabindexes = []
        self.img_data = []
        self.image_ismap_ok = True
        self.svg_data = []
        self.tags_closed = True
        self.duped_attrs = []
        self.no_duped_attrs = True
        self.out_of_landmarks = []
        self.out_of_landmarks_data = []
        self.spelling_errors = False

        # Flags
        self.grab_title = False
        self.grab_svg_desc = False
        self.grab_svg_title = False
        self.grab_link_text = False
        self.skip_script = False
        self.ignore_content = False
        self.svg = False

        # DOM location information
        self.ids = {}
        self.tag_queue = []
        self.tag_data_queue = []
        self.link = self.LINK_DEFAULT
        self.linked_images = []
        self.link_text = []
        self.in_landmarks = []

        # Invisible styling
        self.hidden_attrs = {
            'style': [
                'display:none',
                'visibility:hidden',
                'display:none;visibility:hidden',
                'visibility:hidden;display:none'
            ],
            'aria-hidden': ['true']
        }

        # Blacklists for which tags cannot use aria roles
        base_blacklist = [
            'audio', 'base', 'body', 'button', 'caption', 'col', 'colgroup',
            'datalist', 'dd', 'dt', 'details', 'dialog', 'dl', 'embed', 'form',
            'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hr', 'html',
            'iframe', 'input', 'keygen', 'label', 'map', 'math', 'menuitem',
            'meta', 'meter', 'noscript', 'object', 'ol', 'optgroup', 'option',
            'param', 'picture', 'progress', 'script', 'select', 'source',
            'style', 'SVG', 'summary', 'template', 'textarea', 'title',
            'track', 'ul', 'video'
        ]

        self.main_blacklist = base_blacklist + [
            'address', 'aside', 'section',
        ]
        self.search_blacklist = base_blacklist + [
            'address', 'article',
        ]
        self.banner_blacklist = base_blacklist + [
            'address', 'article', 'aside', 'section',
        ]
        self.navigation_blacklist = base_blacklist + [
            'address', 'article', 'aside', 'section',
        ]
        self.contentinfo_blacklist = base_blacklist + [
            'article', 'aside', 'section',
        ]

        # Lists of tags for specific purposes
        self.content_tags = [
            'a', 'area', 'audio', 'button', 'details', 'div', 'embed', 'form',
            'iframe', 'img', 'input', 'keygen', 'object', 'p', 'rect',
            'select', 'span', 'summary', 'svg', 'table', 'td', 'textarea',
            'tr', 'video',
        ]

    def is_hidden(self, attributes):
        """
        Checks if this element is hidden, using its attributes.

        :param attributes: the list of attribute, value pairs given to
            handle_starttag.
        :return: boolean
        """
        hidden_content = False

        for name, value in attributes:
            if self.hidden_attrs.get(name, False):
                hidden_content = hidden_content or any(
                    [
                        x in value.replace(" ", "")
                        for x in self.hidden_attrs[name]
                    ]
                )

        return hidden_content

    def handle_starttag(self, tag, attributes):

        # Mark tags as they are processed for the queue
        tag_data = {
            'tagname': tag,
            'attributes': attributes,
            'landmark': False,
        }

        self.tag_queue.append(tag)
        self.tag_data_queue.append(tag_data)

        if 'iframe' not in self.tag_queue and tag == "html":

            for name, value in attributes:
                # Check for language aria attribute
                if (
                    name == 'lang' and
                    value is not None and
                    value.strip() != ""
                ):
                    self.role_language = value
                    break

            return

        # We want to ignore everything except these tests in <head>
        if 'head' in self.tag_queue:

            # Grab the title
            if not self.title and tag == 'title':
                self.grab_title = True

            return

        # Screenreaders only focus on elements in the <body> tag
        if 'body' not in self.tag_queue or self.ignore_content:
            return

        # Ignore any hidden elements
        if self.is_hidden(attributes):

            # Hidden container elements should have their content ignored
            if tag in self.content_tags:
                self.ignore_content = True

            return

        # Check for duplicate attributes
        attr_names = [x[0] for x in attributes]
        attr_names.sort()

        attr_names_no_dupes = list(set(attr_names))
        attr_names_no_dupes.sort()

        if attr_names != attr_names_no_dupes:
            self.no_duped_attrs = False
            self.duped_attrs.append(
                [
                    tag,
                    [x for x in attr_names if attr_names.count(x) > 1]
                ]
            )

        # HTML5 native ARIA tags
        native_main = (tag == MAIN)
        native_banner = (tag == BANNER or tag == 'header')
        native_search = (tag == SEARCH)
        native_navigation = (tag == NAVIGATION or tag == 'nav')
        native_contentinfo = (tag == CONTENTINFO or tag == 'footer')

        # Set up for other types of ARIA landmark checks
        html5_main = False
        html5_banner = False
        html5_search = False
        html5_navigation = False
        html5_contentinfo = False

        pre_html5_main = False
        pre_html5_banner = False
        pre_html5_search = False
        pre_html5_navigation = False
        pre_html5_contentinfo = False

        for name, value in attributes:
            # Ensure each element has only one ID (or none)
            if name == 'id':
                if not value:
                    # ID was not set
                    continue

                try:
                    self.ids[value] += 1
                    self.ids_ok = False
                except KeyError:
                    self.ids[value] = 1

            # Check that all tabindexes are negative or 0
            if name == 'tabindex':
                if value.strip() != '' and int(value) > 0:
                    self.bad_tabindexes.append(value)

            # ARIA landmarks with set roles
            if name == 'role':
                html5_main = (tag == value == MAIN)
                html5_banner = (tag == 'header' and value == BANNER)
                html5_search = (tag == 'form' and value == SEARCH)
                html5_navigation = (tag == 'nav' and value == NAVIGATION)
                html5_contentinfo = (tag == 'footer' and value == CONTENTINFO)

                pre_html5_main = (
                    value == MAIN and
                    tag not in self.main_blacklist
                )
                pre_html5_banner = (
                    value == 'banner' and
                    tag not in self.banner_blacklist
                )
                pre_html5_search = (
                    value == SEARCH and
                    tag not in self.search_blacklist
                )
                pre_html5_navigation = (
                    value == NAVIGATION and
                    tag not in self.navigation_blacklist
                )
                pre_html5_contentinfo = (
                    value == CONTENTINFO and
                    tag not in self.contentinfo_blacklist
                )

        # ARIA checks!
        main = native_main or html5_main or pre_html5_main
        banner = native_banner or html5_banner or pre_html5_banner
        search = native_search or html5_search or pre_html5_search
        navigation = (
            native_navigation or
            html5_navigation or
            pre_html5_navigation
        )
        contentinfo = (
            native_contentinfo or
            html5_contentinfo or
            pre_html5_contentinfo
        )

        if main and not any([banner, search, navigation, contentinfo]):
            self.role_main = True
            tag_data['landmark'] = MAIN
            self.in_landmarks.append(MAIN)

        if banner and not any([main, search, navigation, contentinfo]):
            self.role_banner = True
            tag_data['landmark'] = BANNER
            self.in_landmarks.append(BANNER)

        if search and not any([main, banner, navigation, contentinfo]):
            self.role_search = True
            tag_data['landmark'] = SEARCH
            self.in_landmarks.append(SEARCH)

        if navigation and not any([main, banner, search, contentinfo]):
            self.role_nav = True
            tag_data['landmark'] = NAVIGATION
            self.in_landmarks.append(NAVIGATION)

        if contentinfo and not any([main, banner, search, navigation]):
            if ('article' not in self.tag_queue and
                    'section' not in self.tag_queue):
                self.role_contentinfo = True
                tag_data['landmark'] = CONTENTINFO
                self.in_landmarks.append(CONTENTINFO)

        # Handle linked images
        if tag == 'a':
            self.grab_link_text = True
            for name, value in attributes:
                if 'href' in name:
                    self.link = value
                    # Linked <a> tags can't be landmarks
                    tag_data['landmark'] = False

                if name == 'title':
                    self.link_text.append(value)

        # Handle link analytics D:
        if tag == 'script':
            self.skip_script = True

        # Handle iframes
        if tag == 'iframe':
            title_found = False

            for name, value in attributes:
                if name == 'title':
                    title_found = True
                    break

            if not title_found:
                self.bad_iframes.append(
                    strings.create_tag_string(tag, attributes)
                )

        # <li> elements can't be landmarks if in <ol> or <ul>
        if tag == 'li' and ('ol' in self.tag_queue or 'ul' in self.tag_queue):
            tag_data['landmark'] = False

        # <area> and <link> elements can't be landmarks if they have hrefs
        if [attr for attr in attributes if attr[0] == 'href']:
            if tag == 'area' or tag == 'link':
                tag_data['landmark'] = False

        # <menu> with type='toolbar' can't be a landmark
        if tag == 'menu' and ('type', 'toolbar') in attributes:
            tag_data['landmark'] = False

        # Scrape img tag's data
        if tag == 'img' or tag == 'image':
            img_tests = {
                'misspelled_words': [],
                'attrs': {
                    'src': self.SRC_DEFAULT,
                    'alt': self.ALT_DEFAULT,
                }
            }

            for name, value in attributes:
                if value is None:
                    value = self.ATTR_DEFAULT

                img_tests['attrs'][name] = value.strip()

            img_tests['link'] = self.link
            img_tests['link_text'] = " | ".join(self.link_text)

            if self.grab_link_text:
                self.linked_images.append(img_tests)

            # Check spelling of alt text
            img_alt = img_tests['attrs']['alt']

            if img_alt != self.ALT_DEFAULT:
                for word in img_alt.split(" "):
                    if word != "" and not self.dict.check(word):
                        img_tests['misspelled_words'].append(word)
                        self.spelling_errors = True

            # Images with blank alts can't be landmarks
            if img_alt in ["", self.ATTR_DEFAULT, self.ALT_DEFAULT]:
                tag_data['landmark'] = False

            attrs = list(img_tests.keys())

            # Check for incorrect ismap/usemap usage
            if "ismap" in attrs and "usemap" not in attrs:
                self.image_ismap_ok = False

            self.img_data.append(img_tests)

        # Handle SVGs
        if self.svg:
            if tag == 'title':
                self.grab_svg_title = True

            if tag == 'desc':
                self.grab_svg_desc = True

            self.svg['attrs']['inner_html'] += strings.create_tag_string(
                tag,
                attributes
            )

        if tag == 'svg':
            svg_tag = strings.create_tag_string(tag, attributes)

            self.svg = {
                'misspelled_words': [],
                'link': self.link,
                'link_text': " | ".join(self.link_text),
                'attrs': {
                    'tag': svg_tag,
                    'title': self.TITLE_DEFAULT,
                    'desc': "<em>No desc.</em>",
                    'inner_html': "",
                },
            }

        # Find potentially important data outside of ARIA landmarks
        if not self.in_landmarks and tag in self.content_tags:
            self.out_of_landmarks.append(tag)
            self.out_of_landmarks_data.append(
                strings.create_tag_string(tag, attributes)
            )

    def handle_data(self, data):
        # Grab the title of the page
        if self.grab_title:
            self.title = data
            self.grab_title = False

        # Handle SVG stuff
        if self.svg:
            self.svg['attrs']['inner_html'] += data

        if self.grab_svg_title:
            self.svg['attrs']['title'] = data
            self.grab_svg_title = False

        if self.grab_svg_desc:
            self.svg['attrs']['desc'] = data

            # Check spelling
            for word in data.split(" "):
                if word != "" and not self.dict.check(word):
                    self.svg['misspelled_words'].append(word)
                    self.spelling_errors = True

            self.grab_svg_desc = False

        if self.grab_link_text and not self.skip_script:
            stripped_data = data.strip()

            if stripped_data != '':
                self.link_text.append(stripped_data)

                for img in self.linked_images:
                    img['link_text'] = " | ".join(self.link_text)

    def handle_endtag(self, tag):
        # Check for non-closed tags
        try:
            self.tag_queue.remove(tag)
        except ValueError:
            print("Extra end tag found: " + tag)
            return  # Extra end-tags are OK, i guess...

        last_tag_data = self.tag_data_queue[-1]

        # Check if we're leaving an ARIA landmark
        if last_tag_data['tagname'] == tag and last_tag_data['landmark']:
            self.in_landmarks.remove(last_tag_data['landmark'])

        # Check if we're leaving a hidden section
        if (
            self.ignore_content and
            tag in self.content_tags and
            last_tag_data['tagname'] == tag
        ):
            if self.is_hidden(last_tag_data['attributes']):
                self.ignore_content = False

        if self.strip_void_elements(self.tag_queue):
            self.tags_closed = False
        else:
            self.tags_closed = True

        # Handle SVGs
        if self.svg:
            self.svg['attrs']['inner_html'] += "</" + tag + ">"

            if tag == "svg":
                # Handle weird duplication bug?
                if self.svg not in self.svg_data:
                    self.svg_data.append(self.svg)

                self.svg = False

        # Handle linked images
        if tag == 'a':
            self.link = self.LINK_DEFAULT
            self.link_text = []
            self.linked_images = []
            self.grab_link_text = False

        # Handle link analytics
        if tag == 'script':
            self.skip_script = False

        self.tag_data_queue.remove(last_tag_data)

    def get_duped_ids(self):
        """
        Generates and returns a list of all IDs which showed up more
        than once on the page.

        :return: an array of all IDs that appeared more than once.
        """
        return [x[0] for x in list(self.ids.items()) if x[1] > 1]

    def get_duped_attr_string(self):
        """
        Generates a string to include on the report page for all the
        tags with duplicated attributes on the parsed page.

        :return: a string that shows <tagname duped-attr>.
        """
        duped_str = ""
        for tagname, attrs in self.duped_attrs:
            duped_str += strings.create_tag_string(tagname, attrs)

        return duped_str

    @staticmethod
    def strip_void_elements(tags):
        """
        Removes self-closing tags that are given a pass by accessibility from a
        list of tags.

        :param tags: the list of tags to examine.
        :return: the list of tags without any self-closing tags.
        """
        ok_tags = [
            'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img',
            'input', 'keygen', 'link', 'meta', 'param', 'source',
            'track', 'wbr'
        ]

        return [t for t in tags if t not in ok_tags]
