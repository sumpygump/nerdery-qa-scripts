import http.client
from urllib.parse import urlencode


def send_text(numstring, message):
    """
    Sends a given message to the given phone number as a text message.

    :param numstring: the phone number to send the text to, as a string.
    :param message: the message to send. Also a string.
    :return: a tuple, (response status, response reason).
    """
    params = urlencode({'number': numstring, 'message': message})
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}

    conn = http.client.HTTPConnection("textbelt.com:80")
    conn.request("POST", "/text", params, headers)
    response = conn.getresponse()

    return response.status, response.reason
