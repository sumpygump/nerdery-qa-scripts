import re
from time import sleep

import requests

from requests.auth import HTTPBasicAuth
from requests.auth import HTTPDigestAuth
from requests.exceptions import ChunkedEncodingError
from requests.exceptions import ConnectionError
from requests.exceptions import InvalidSchema
from requests.exceptions import InvalidURL
from requests.exceptions import ReadTimeout
from requests.exceptions import TooManyRedirects
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import UnexpectedAlertPresentException
from selenium.common.exceptions import WebDriverException

from . import authentication
from . import logging
from .decorators import deprecated


HANDLED_ERRORS = (
    ChunkedEncodingError, ConnectionError, InvalidSchema, InvalidURL,
    ReadTimeout, TooManyRedirects
)


def source_is_html(source):
    """
    Checks to see if the source starts with "<", which means it might be
    HTML.

    :param source: a string that might be HTML.
    :return: True if source is HTML; False otherwise.
    """
    if not source:
        return False

    is_html = False

    try:
        if source.startswith('<'):
            is_html = True
    except TypeError:
        if source.startswith(b'<'):
            is_html = True
    except AttributeError:
        # Source is not a string, definitely not HTML.
        pass

    return is_html


def get_requests_auth_pack(auth):
    """
    Turns an auth dict into an auth pack for Requests.

    :param auth: an auth dict, such as the one made by get_auth_dict().
    :return: an HTTPBasicAuth or HTTPDigestAuth object, or None.
    """
    if not auth:
        auth_pack = None
    elif auth['authtype'] == 'BASIC':
        auth_pack = HTTPBasicAuth(auth['username'], auth['password'])
    elif auth['authtype'] == 'DIGEST':
        auth_pack = HTTPDigestAuth(auth['username'], auth['password'])
    else:
        auth_pack = None

    return auth_pack


def get_response_safely(url, auth=None, header={}, follow=True):
    """
    A wrapper for requests to catch exceptions and return a more useful
    thing.

    :param url: the url to open safely.
    :param auth: an auth dict with authtype, username, and password, if
                 the call requires auth.
    :param header: any custom headers that need to be included. This will be
                   a string that looks like "Authorization: Bearer 21341vx".
    :param follow: whether or not to follow 30x responses.
    :return: A list - [entered_url, final_url, status_code]
    """
    try:
        auth_pack = None

        headers = requests.utils.default_headers()
        headers.update(header)
        headers.update(
            {
                'User-Agent': "The Nerdery's Spiderer",
                'From': "pgoy@nerdery.com"
            }
        )

        if auth:
            auth_pack = get_requests_auth_pack(auth)

        if follow:
            response = requests.get(
                url, auth=auth_pack, headers=headers, timeout=20
            )
        else:
            response = requests.head(
                url, auth=auth_pack, headers=headers, timeout=20
            )

        return [url, response.url, response.status_code]

    except HANDLED_ERRORS as e:
        msg = "{0} get_response_safely: '{1}' at {2}\n".format(
            logging.get_logtime(), e, url)

        logging.log(msg)

        return [url, url, "ERR"]


def get_source_with_requests(url, auth=None):
    """
    Gets the page source using requests.

    :param url: the URL to request.
    :param auth: the auth dict, if auth is required. Default is None.
    :return: a map of the final URL and the source of the page.
    """
    actual_url = url

    headers = requests.utils.default_headers()
    headers.update(
        {
            'User-Agent': "The Nerdery's Spiderer",
            'From': "pgoy@nerdery.com"
        }
    )

    try:
        auth_pack = get_requests_auth_pack(auth)

        response = requests.get(
            url, auth=auth_pack, headers=headers, timeout=20
        )

        actual_url = response.url
        raw_source = response.content.strip()

    except HANDLED_ERRORS as e:
        msg = "{0} get_source_with_requests: '{1}' at {2}\n".format(
            logging.get_logtime(), e, url)

        logging.log(msg)

        raw_source = None

    if raw_source:
        try:
            source = str(raw_source, 'utf-8')

        except UnicodeDecodeError:
            try:
                source = str(raw_source, 'latin1')

            except UnicodeDecodeError:
                msg = ("{0} get_source_with_requests: "
                       "Unable to decode source of {1}\n")
                msg = msg.format(logging.get_logtime(), actual_url)

                logging.log.write(msg)

                source = None
    else:
        source = None

    return {
        'url': actual_url,
        'source': source
    }


def fix_iframe_source(source):
    """
    Fixes the iframe sources by replacing the &lt; and &gt; with < and >,
    respectively.

    :param source: the source of the page.
    :return: the fixed source.
    """
    return source.replace("&lt;", "<").replace("&gt;", ">")


@deprecated
def get_iframe_source(driver, source, cur_depth=0):
    """
    Grabs the iframe source and inserts it into the source where it
    belongs. Intended to be used recursively.

    :param driver: the driver to use to get the source.
    :param source: the current HTML source, as a string.
    :param cur_depth: how deep down the rabbit hole we've gone.
    :return: the modified source string.
    """

    # Selenium now sees all the iframe sources, so the code below is no
    # longer needed. Keeping it just in case this changes back in the
    # future.
    if cur_depth > 10:
        return source

    num_frames = len(driver.find_elements_by_tag_name("iframe"))
    frame_pat = "<iframe.*?></iframe>"

    for frame_num in range(num_frames):
        try:
            cur_frame = driver.find_elements_by_tag_name("iframe")[frame_num]
        except IndexError:
            continue
        except WebDriverException:
            driver.switch_to_default_content()

        try:
            driver.switch_to_frame(cur_frame)

            frame_source = driver.page_source.strip()
            more_frames = driver.find_elements_by_tag_name("iframe")
        except WebDriverException:
            continue

        if more_frames:
            frame_source = get_iframe_source(
                driver,
                frame_source,
                cur_depth + 1
            )

        empty_iframe = re.search(frame_pat, source)
        if empty_iframe:
            insert_ind = empty_iframe.end() - 9

            source = source[:insert_ind] + frame_source + source[insert_ind:]

        driver.switch_to.parent_frame()

    return source


def get_source_with_driver(driver, url=None, wait=None):
    """
    Safely gets the page source while using a webdriver.

    :param driver: the driver to use to go to the supplied URL.
    :param url: the URL to go to and grab the source, if not already there.
    :param wait: if present, how long to wait after the page is loaded.
    :return: a map of the final URL and the source of the page.
    """
    # Note: the "or url"s below are because sometimes driver.current_url
    # can be None. This is a bug with Selenium.
    try:
        if url:
            driver.get(url)

        if wait:
            sleep(int(wait))

        url_data = {
            'url': driver.current_url or url,
            'source': fix_iframe_source(driver.page_source.strip())
        }

    except UnexpectedAlertPresentException:
        try:
            alert = driver.switch_to_alert()
            alert.dismiss()
        except NoAlertPresentException:
            pass

        url_data = {
            'url': driver.current_url or url,
            'source': fix_iframe_source(driver.page_source.strip())
        }

    except (TimeoutException, WebDriverException):
        url_data = {
            'url': url,
            'source': None
        }

    return url_data


def get_driver(args, start_url=None):
    """
    Gets a driver set up with the desired attributes specified in args.

    :param args: the argparse instance for this run.
    :param start_url: the URL to start at. Default is None.
    :return: a new WebDriver.Firefox() instance.
    """
    height = args.height if 'height' in args else 1177
    width = args.width if 'width' in args else 1080

    # # Turn off gif animation
    # fp = webdriver.FirefoxProfile()
    # fp.set_preference("image.animation_mode", "none")

    # driver = webdriver.Firefox(firefox_profile=fp)
    driver = webdriver.Chrome()
    driver.set_window_size(width, height)

    if getattr(args, 'login', False):
        authentication.wait_for_login_with_driver(driver, start_url)

    return driver


def get_driver_or_none(args, start_url=None):
    """
    Returns a driver, or not, depending on what flags are set.

    :param args: the argparse instance for this run.
    :param start_url: the URL to start at. Default is None.
    :return: a new WebDriver.Firefox() instance, or None.
    """
    login = args.login if 'login' in args else False
    js = args.js if 'js' in args else False
    focustest = args.focustest if 'focustest' in args else False

    if login or js or focustest or start_url == 'stepped':
        driver = get_driver(args, start_url)
    else:
        driver = None

    return driver
