"""Handles interactions with strings."""


def add_terminating_slash(s):
    """
    Adds a terminating / to the string if it doesn't have one.

    :param s: the string to fix up.
    :return: the string with a '/' at the end, if it didn't have one already.
    """
    if not s.endswith("/"):
        s = s + "/"

    return s


def remove_terminating_slash(s):
    """
    Removes the terminating / from the string if it exists.

    :param s: the string to fix up.
    :return: the string without a '/' at the end, if it had one.
    """
    if s.endswith("/"):
        s = s[:-1]

    return s


def escape_braces(s):
    """
    Escapes the {s and the }s from strings, to avoid .format() surprises.

    :param s: the string to escape.
    :return: the string with its {}s escaped.
    """
    return s.replace("{", "{{").replace("}", "}}") if s is not None else ''


def create_tag_string(tag, attributes):
    """
    Creates a string that looks like "<tag attr=val>", given a tagname
    and a list of lists of attributes.

    :param tag: the name of the tag
    :param attributes: a list of pairs of attribute names and values
    :return: a string
    """
    spacer = ' ' if len(attributes) > 0 else ''

    return (
        "<{}" + spacer + ' '.join(
            ['{}="{}"'.format(x[0], escape_braces(x[1])) for x in attributes]
        ) + ">"
    ).format(tag)
