import getpass

from .urls import get_base


def create_basic_auth_url(url, username, password):
    """
    Creates a basic auth URL by crammin' the username and password before
    the domain.

    :param url: the URL to go to with basic auth.
    :param username: the username to log in with.
    :param password: the password to log in with.
    :return: the formatted, basic-auth ready URL.
    """
    url_info = get_base(url)
    url_info['username'] = username
    url_info['password'] = password
    url_frmt = "{protocol}://{username}:{password}@{subdomain}.{domain}{path}"

    return url_frmt.format(**url_info)


def get_user_password(username):
    """
    Retrieves the user's password.

    :param username: the username of the account
    :return: string
    """
    pwd_s = "Enter the password for user '{0}': ".format(username)

    return getpass.getpass(pwd_s)


def get_auth_dict(args):
    """
    Creates and returns an auth dict to use with urllib's opener.

    :param args: the parsed args from ArgParser.
    :return: a dictionary of authtype, username, and password.
    """
    if args.username is not None and args.password:
        auth_dict = {
            'authtype': 'DIGEST' if args.digest else 'BASIC',
            'username': args.username,
            'password': get_user_password(args.username)
        }
    else:
        auth_dict = None

    return auth_dict


def wait_for_login_with_requests():
    """
    Waits for the user to login using a separate browser and provide the
    authorization header.

    :return: the raw string as entered by the user.
    """
    msg = ("************************************************************\n"
           " You have indicated you need to log in to access this site. \n"
           " Will you be using a header, a cookie, or both?\n")
    choice = input(msg).lower()

    header_msg = (
        "Please log in using a browser and retrieve the authorization\n"
        " header, then enter it below. It should look something like:\n"
        "               Basic aLphANumEriC234sTRinG555               \n")
    cookie_msg = (
        "Please log in using a browser and retrieve the authorization\n"
        " cookie or cookies, then enter them below.\n")

    auth = {'Cookie': None, 'Authorization': None}

    while True:
        if 'cookie' in choice:
            auth['Cookie'] = input(cookie_msg)
            break
        elif 'header' in choice:
            auth['Authorization'] = input(header_msg)
            break
        elif 'both' in choice:
            auth['Cookie'] = input(cookie_msg)
            auth['Authorization'] = input(header_msg)
            break
        else:
            choice = input(
                "I didn't understand your selection.\n"
                "Please enter 'header', 'cookie', or 'both'.").lower()

    return auth


def wait_for_login_with_driver(driver, url):
    """
    Waits for the user to log in, displaying a helpful message.

    :param driver: the driver to use to go to the URL.
    :param url: the URL to go to for the user to log in.
    """
    login_msg = ("================================================\n"
                 "You have indicated that logging in is required. \n"
                 "      Please log in to the site now, then       \n"
                 "    press 'Enter' after you have logged in.     \n"
                 "================================================\n")
    print(login_msg)
    if url:
        driver.get(url)

    input()
    print("OK, starting test!\n")
