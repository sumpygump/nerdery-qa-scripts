#!/usr/local/bin/python3


import argparse
import os
import sys
from time import sleep

from common import communication
from common import delegation
from common import files
from common import internet
from common import logging


parser = argparse.ArgumentParser(
    description=(
        "Visits each URL in a file of URLs (or the provided URL) and takes a "
        "screenshot of the page."
    )
)
parser.add_argument(
    "url_file",
    help="the file containing all URLs to screenshot."
)
delegation.add_defaults_to_parser(
    parser,
    ['u', 'p', 'basic', 'digest', 'login', 'wait',
     'height', 'width', 'o', 'textme']
)


def grab_screenshots(args):
    """
    Takes a file of URLs and goes to each, taking a screenshot of each
    page.

    :param args: the dictionary of parsed arguments from argparse.
    """
    now = logging.get_logtime()
    wait = float(args.wait) if args.wait else None

    urls = files.get_url_list(args.url_file)
    path = args.output or os.getcwd()

    if not os.path.exists(path):
        os.makedirs(path)

    filename_tmp = "{dir}/{url}_{date}_screengrab.png"  # formatted in for-loop
    driver = internet.get_driver(args, urls[0])

    for url in urls:
        url_filename = files.get_filename_from_url(url)

        screenshot_name = filename_tmp.format(
            dir=path,
            url=url_filename,
            date=now)

        driver.get(url)

        if wait:
            sleep(wait)

        driver.get_screenshot_as_file(screenshot_name)

    driver.quit()
    completed_message = "Screenshot grab complete!"

    if args.textme:
        communication.send_text(args.textme, completed_message)

    print(completed_message)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        args = parser.parse_args()
        grab_screenshots(args)
    else:
        parser.print_help()
else:
    print("This module is meant to be used as a standalone script.")
