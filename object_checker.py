#!/usr/local/bin/python3

import argparse
from collections import defaultdict
from operator import itemgetter
import sys

from common import authentication
from common import delegation
from common import internet
from common import files
from common import parsers


parser = argparse.ArgumentParser(
    description=(
        "Using a .txt file of URLs, checks every URL and outputs each element "
        "on each URL that matched the search criteria provided (either the "
        "element specified appeared or did not appear)."
    )
)
parser.add_argument(
    "url_file",
    help="the file containing all URLs to check."
)
parser.add_argument(
    "-t", "--tagname",
    help="specify the tag name to search for, such as 'div'."
)
parser.add_argument(
    "-a", "--attrs", action='append',
    help=("specify an attribute,value pair (comma-separated). This argument "
          "can be used multiple times in a single call to specify that the "
          "tag should have each of these attribute,value pairs.")
)
parser.add_argument(
    "-c", "--content", action='append',
    help=("specify something to search for within the content of the tag. For "
          "example, <p>Thing to find</p> can be found with `-c Thing`. This "
          "argument can be used multiple times to specify that the tag should "
          "contain each of the specified contents.")
)
parser.add_argument(
    "--partial", action='store_true',
    help=("indicate that the attribute,value is a partial match. For example, "
          "`-t img -a src,https --partial` would find all 'img' tags whose "
          "'src' attribute's value contained 'https'.")
)
parser.add_argument(
    "-i", "--invert", action='store_true',
    help=("instead, return all objects which DO NOT have the specified "
          "attribute,value pair."),
    default=False
)
parser.add_argument(
    "--presence", action='store_true',
    help="only report each page which has the object.",
    default=False
)
parser.add_argument(
    "--nonpresence", action='store_true',
    help="only report each page which does NOT have the object.",
    default=False
)
parser.add_argument(
    "-v", "--verbose", action='store_true',
    help="ask object_checker to provide a more detailed output."
)
parser.add_argument(
    "--unique", action='store_true',
    help=(
        "reports each unique element found in the list of URLs, and which "
        "URLs they appeared on. Also provides a list of the smallest number "
        "of URLs one would need to visit to see examples of each element."
    )
)
delegation.add_defaults_to_parser(
    parser,
    ['u', 'p', 'basic', 'digest', 'threads', 'js', 'login',
     'wait', 'height', 'width', 'o', 'textme']
)


def get_unique_minimal_span(element_locations):
    """
    Gets the minimal list of URLs to visit to see examples of each of the
    unique elements provided.

    :param element_locations: a dictionary of element: [urls]
    :return: a list of URL strings
    """
    elements = sorted(element_locations.keys())

    urls = set(
        [url for url_list in element_locations.values() for url in url_list]
    )

    url_elements = defaultdict(list)

    for url in urls:
        url_elements[url] = [
            ele for ele in elements if url in element_locations[ele]
        ]

    url_counts = [
        (url, len(url_elements[url])) for url in urls
    ]

    minimal_span = []

    for url, count in sorted(url_counts, key=itemgetter(1), reverse=True):
        seen_elements = set([
            ele for u in minimal_span for ele in url_elements[u]
        ])

        new_elements = set([
            ele for ele in url_elements[url]
        ])

        if seen_elements.union(new_elements) != seen_elements:
            minimal_span.append(url)

        if elements == sorted(seen_elements.union(new_elements)):
            break

    return minimal_span


def get_output_string(
    url_object_data,
    tag,
    pairs=None,
    content=None,
    presence=False,
    nonpresence=False,
    invert=False,
    verbose=False,
    unique=False
):
    """
    Performs the complicated checks to figure out how to format the
    report, using all the flags the user gave.

    :param url_object_data: all the urls and their objects to report on.
    :param pairs: attribute,value pairs, as a list of lists
    :param content: specific content string
    :param presence: flag to only report each page that had the object
    :param nonpresence: flag to only report each page that did not have
        the object
    :param invert: flag to report all the objects that DO NOT have the
        specified attribute,value pair
    :param verbose: flag to provide more information than just the URLs.
    :param unique: flag to check for unique tags.
    :return: the final string to print.
    """
    output_str = ""

    for url, objects in url_object_data:
        msg = ""

        if nonpresence:
            if not presence:
                if not verbose:
                    msg = '{0}\n'.format(url)

                else:
                    msg = '{0} did not have a {1}'.format(url, tag)

                    if pairs:
                        msg += " with {0}".format(pairs)

                    if content:
                        msg += " containing {0}".format(content)

                    msg += ".\n"

        elif len(objects) > 0:
            msg = '{0}\n'.format(url)

            if presence:
                if invert:
                    if verbose:
                        msg = '{0} had {1} object(s) different than {2}'
                        msg = msg.format(url, len(objects), tag)

                        if pairs:
                            msg += " with {0}".format(pairs)

                        if content:
                            msg += " containing {0}".format(content)

                        msg += ".\n"

                else:
                    if verbose:
                        msg = '{0} had a {1}'.format(url, tag)

                        if pairs:
                            msg += " with {0}".format(pairs)

                        if content:
                            msg += " containing {0}".format(content)

                        msg += ".\n"

            else:
                if verbose and not unique:
                    msg = "\nOn {0}:\n".format(url)

                    for obj in sorted(objects):
                        if content:
                            msg += "    {0}\n".format(obj)
                        else:
                            msg += "    <{0} {1}>\n".format(tag, obj)

                # In this case, "url" is actually an element, and
                # "objects" is a list of URLs that element appeared on.
                elif unique:
                    element = url

                    msg = "\n<{0}>:\n".format(element)

                    for url in sorted(objects):
                        msg += "    {0}\n".format(url)

        output_str += msg

    return output_str


def check_for_objects(args):
    """
    Grabs the sources of each page from the provided URL file and checks
    for the given tagname(s) that match the provided constraints. Writes
    results to a text file in the current directory.

    :param args: the dictionary of parsed arguments from argparse.
    """
    if args.tagname is None:
        print("Must specify a tagname to search for! (Use -t or --tagname.)")
        sys.exit(1)

    tag = args.tagname
    urls = files.get_url_list(args.url_file)
    auth = authentication.get_auth_dict(args)
    file = args.output or "object_checker_report.txt"
    pairs = [x.split(',') for x in args.attrs] if args.attrs else None
    content = args.content
    wait = float(args.wait) if args.wait else None

    if args.js or args.login:
        sources = []

        driver = internet.get_driver_or_none(args, urls[0])

        for url in urls:
            sources.append(internet.get_source_with_driver(
                driver,
                url,
                wait=wait)
            )

        driver.quit()

    else:
        sources = delegation.get_with_threaded_map(
            lambda url: internet.get_source_with_requests(url, auth),
            urls,
            num_threads=int(args.threads)
        )

    parsed_data = []

    if args.unique:
        parsed_elements = defaultdict(list)

    # Write out the results with a complicated set of checks and balances.
    for source in sources:
        url = source['url']
        html = source['source']

        if html is None:
            continue

        parser = parsers.ObjectParser(
            tag,
            pairs,
            content,
            args.partial,
            args.invert
        )

        parser.feed(html)

        if args.unique:
            new_objs = set(parser.bad_objects)

            for obj in new_objs:
                parsed_elements[obj].append(url)

        else:
            parsed_data.append(
                [url, parser.bad_objects]
            )

    if args.unique:
        parsed_data = [[key, value] for key, value in parsed_elements.items()]

    with open(file, 'w+') as report:
        output_str = get_output_string(
            parsed_data,
            tag,
            pairs=pairs,
            content=content,
            presence=args.presence,
            nonpresence=args.nonpresence,
            invert=args.invert,
            verbose=args.verbose,
            unique=args.unique
        )

        if args.unique:
            minimal_span = get_unique_minimal_span(parsed_elements)

            output_str += (
                "\n\nVisit these URLs to see examples of each of the above "
                "unique elements:\n"
            )

            for url in minimal_span:
                output_str += "    " + url + "\n"

        report.write(output_str)

    print("Done!")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        args = parser.parse_args()

        check_for_objects(args)
    else:
        parser.print_help()
else:
    print("This module is meant to be used as a standalone script.")
